// @flow
import { combineReducers } from 'redux';
import { authReducer } from '../modules/auth';
import { movieReducer } from '../modules/addOrEditMovie';
// import { cinemaReducer } from '../modules/cinemaCreation';
// import { roomsReducer } from '../modules/roomList';
import { additionReducer } from '../modules/addition';
// import cinemaPageReducer from '../modules/cinemaPage/reducers/cinemaPageReducer';
import movieListReducer from '../modules/movieList/reducers/movieListReducer';
// import sessionListReducer from '../modules/sessionList/reducers/sessionListReducer';
// import { sessionCreationReducer } from '../modules/sessionCreation/';
import cinemaListReducer from '../modules/cinemaList/reducers/cinemaListReducer';

// import { roomSchemaReducer } from '../modules/roomPage/seatSchemaView/index';

const rootReducer = combineReducers({
  auth: authReducer,
  movie: movieReducer,
  // rooms: roomsReducer,
  // cinema: cinemaReducer,
  movies: movieListReducer,
  cinemas: cinemaListReducer,
  // cinemaPage: cinemaPageReducer,
  // sessions: sessionListReducer,
  // session: sessionCreationReducer,
  addition: additionReducer,
  // roomSchema: roomSchemaReducer,
});

export default rootReducer;
