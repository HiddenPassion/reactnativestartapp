// @flow
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
 compose, onlyUpdateForKeys, setPropTypes, withHandlers,
} from 'recompose';
import { Alert } from 'react-native';
import Movie from './movie';
import { deleteMovie } from '../movieList/thunk';

const MovieContainer = ({ movie, deleteMovieHandler, editMovieHandler }) => (
  <Movie
    movie={movie}
    onEditButtonPress={editMovieHandler}
    onDeleteButtonPress={deleteMovieHandler}
  />
);

MovieContainer.navigatorStyle = {
  tabBarHidden: true,
};

const mapDispatchToProp = dispatch => ({
  deleteMovieById: id => dispatch(deleteMovie(id)),
});

export default compose(
  onlyUpdateForKeys(['movie']),
  setPropTypes({
    movie: PropTypes.object.isRequired,
  }),
  connect(
    null,
    mapDispatchToProp,
  ),
  withHandlers({
    editMovieHandler: ({ movie, navigator }) => () => {
      navigator.push({
        screen: 'navigation.editMoviePage',
        title: `Editing ${movie.name}`,
        passProps: {
          movieId: movie.id,
        },
      });
    },
    deleteMovieHandler: ({ movie, deleteMovieById, navigator }) => () => {
      Alert.alert(`You really want delete ${movie.name}`, '', [
        {
          text: '               Yes               ',
          onPress: () => {
            deleteMovieById(movie.id);
            navigator.pop();
          },
        },
        {
          text: '               No                ',
        },
      ]);
    },
  }),
)(MovieContainer);
