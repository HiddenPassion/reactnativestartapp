// @flow
import React from 'react';
import {
  Text, View, Image, StyleSheet, ScrollView, TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Ionicons';

type Props = {
  movie: {
    poster: string,
    name: string,
    length: number,
    actors: string,
    yearRelease: number,
    description: string,
  },
  onEditButtonPress: Function,
  onDeleteButtonPress: Function,
};

const Movie = ({ movie, onEditButtonPress, onDeleteButtonPress }: Props) => (
  // eslint-disable-next-line
  <ScrollView style={{ flex: 1 }}>
    <View style={styles.container}>
      <Image resizeMode="stretch" source={{ uri: movie.poster }} style={styles.image} />
      <View style={styles.headerContainer}>
        <Text style={styles.header}>{movie.name}</Text>
      </View>
      <View style={styles.controlButtons}>
        <TouchableOpacity onPress={onEditButtonPress}>
          {/* eslint-disable-next-line */}
          <Icon name="md-create" size={30} color="blue" style={{ marginRight: 70 }} />
        </TouchableOpacity>
        <TouchableOpacity onPress={onDeleteButtonPress}>
          <Icon name="md-trash" size={30} color="red" />
        </TouchableOpacity>
      </View>
      <View style={styles.descriptionContainer}>
        <View style={styles.parametrsContainer}>
          <Text style={styles.defaultText}>
            <Text style={styles.description}>Length: </Text>
            {movie.length} min.
          </Text>
          <Text style={styles.defaultText}>
            <Text style={styles.description}>Actors: </Text>
            {movie.actors}
          </Text>
          <Text style={styles.defaultText}>
            <Text style={styles.description}>Year release: </Text>
            {movie.yearRelease}
          </Text>
        </View>
        <Text style={styles.defaultText}>{movie.description}</Text>
      </View>
    </View>
  </ScrollView>
);

Movie.propTypes = {
  movie: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 15,
    backgroundColor: '#eee',
    alignItems: 'center',
  },
  image: {
    width: '90%',
    height: 350,
  },
  headerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    fontSize: 24,
    fontWeight: '800',
  },
  controlButtons: {
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  descriptionContainer: {
    width: '90%',
  },
  parametrsContainer: {
    marginVertical: 10,
    marginLeft: 15,
  },
  description: {
    fontSize: 18,
    fontWeight: '500',
  },
  defaultText: {
    fontSize: 16,
  },
});

export default Movie;
