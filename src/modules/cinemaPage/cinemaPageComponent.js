// @flow
import React from 'react';
import {
 ScrollView, View, Text, Image, StyleSheet, TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Addition from '../addition/additionList';
import AsignMovies from '../asignMovies/asignMovies';

type Cinema = {
  imageUrl: string,
  name: string,
  city: string,
  address: string,
  description: string,
  contactNumber: string,
  imageUrl: string,
  id: string,
};

type Props = {
  cinema: Cinema,
  onDeleteButtonPress: Function,
};

const cinemaPage = (props: Props) => {
  const { cinema, onDeleteButtonPress } = props;
  return (
    <ScrollView style={{ flex: 1 }}>
      <View style={styles.container}>
        <Image resizeMode="stretch" source={{ uri: cinema.imageUrl }} style={styles.image} />
        <View style={styles.headerContainer}>
          <Text style={styles.header}>{cinema.name}</Text>
        </View>
        <View style={styles.controlButtons}>
          <TouchableOpacity onPress={() => console.log('onEditButtonPress')}>
            {/* eslint-disable-next-line */}
            <Icon name="md-create" size={30} color="blue" style={{ marginRight: 70 }} />
          </TouchableOpacity>
          <TouchableOpacity onPress={onDeleteButtonPress}>
            <Icon name="md-trash" size={30} color="red" />
          </TouchableOpacity>
        </View>
        <View style={styles.descriptionContainer}>
          <View style={styles.parametrsContainer}>
            <Text style={styles.defaultText}>
              <Text style={styles.description}>City: </Text>
              {cinema.city}
            </Text>
            <Text style={styles.defaultText}>
              <Text style={styles.description}>Address: </Text>
              {cinema.address}
            </Text>
            <Text style={styles.defaultText}>
              <Text style={styles.description}>Contact number: </Text>
              {cinema.contactNumber}
            </Text>
          </View>
          <Text style={styles.defaultText}>{cinema.description}</Text>
        </View>
        <View style={styles.additionContainer}>
          {/* eslint-disable-next-line */}
          <View style={{ marginBottom: 20 }}>
            {/* eslint-disable-next-line */}
            <Text style={{ fontWeight: 'bold', fontSize: 24 }}>Additions</Text>
          </View>
          <Addition cinemaId={cinema.id} />
        </View>
        <AsignMovies cinemaId={cinema.id} />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 15,
    backgroundColor: '#eee',
    alignItems: 'center',
  },
  image: {
    width: '90%',
    height: 350,
  },
  headerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    fontSize: 24,
    fontWeight: '800',
  },
  controlButtons: {
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  descriptionContainer: {
    width: '90%',
  },
  parametrsContainer: {
    marginVertical: 10,
    marginLeft: 15,
  },
  description: {
    fontSize: 18,
    fontWeight: '500',
  },
  defaultText: {
    fontSize: 16,
  },
  additionContainer: {
    width: '95%',
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 50,
    borderWidth: 1,
    borderColor: '#ccc',
    backgroundColor: '#f9f9f9',
  },
});

export default cinemaPage;
