// @flow
import React from 'react';
import { compose, withHandlers, onlyUpdateForKeys } from 'recompose';
import { connect } from 'react-redux';
import { Alert } from 'react-native';
import CinemaPageComponent from './cinemaPageComponent';
import { deleteCinema } from '../cinemaList/thunk';

type Props = {
  cinema: Object,
  deleteCinemaHandler: Function,
};

const CinemaPage = (props: Props) => {
  const { cinema, deleteCinemaHandler } = props;
  return <CinemaPageComponent cinema={cinema} onDeleteButtonPress={deleteCinemaHandler} />;
};

const mapDispatchToProps = (dispatch: Function) => ({
  deleteCinema: (id: string) => dispatch(deleteCinema(id)),
});

export default compose(
  onlyUpdateForKeys(['cinema']),
  connect(
    null,
    mapDispatchToProps,
  ),
  withHandlers({
    deleteCinemaHandler: ({ cinema, deleteCinema, navigator }) => () => {
      Alert.alert(`You really want delete ${cinema.name}`, '', [
        {
          text: '               Yes               ',
          onPress: () => {
            deleteCinema(cinema.id);
            navigator.pop();
          },
        },
        {
          text: '               No                ',
        },
      ]);
    },
  }),
)(CinemaPage);
