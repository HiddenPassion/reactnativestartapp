// @flow
import * as actionTypes from './actionTypes';

const addingAdditionStart = () => ({
  type: actionTypes.ADDING_ADDITION_START,
});

const addingAdditionFail = (data: Object) => ({
  type: actionTypes.ADDING_ADDITION_FAIL,
  error: data,
});

const addingAdditionSuccess = (data: Object) => ({
  data: data.data.data,
  type: actionTypes.ADDING_ADDITION_SUCCESS,
});

const editingAdditionStart = () => ({
  type: actionTypes.EDITING_ADDITION_START,
});

const editingAdditionFail = (data: Object) => ({
  type: actionTypes.EDITING_ADDITION_FAIL,
  error: data,
});

const editingAdditionSuccess = (data: Object) => ({
  data: data.data.data,
  type: actionTypes.EDITING_ADDITION_SUCCESS,
});

const receivingAdditionsStart = () => ({
  type: actionTypes.RECEIVING_ADDITIONS_START,
});

const receivingAdditionsSuccess = (data: Object) => ({
  type: actionTypes.RECEIVING_ADDITIONS_SUCCESS,
  data: data.data.data,
});

const receivingAdditionsFail = (err: Object) => ({
  type: actionTypes.RECEIVING_ADDITIONS_FAIL,
  error: err,
});

const deletingAdditonStart = () => ({
  type: actionTypes.DELETING_ADDITION_START,
});

const deletingAdditionSuccess = (id: string) => ({
  id,
  type: actionTypes.DELETING_ADDITION_SUCCESS,
});

const deletingAdditionFail = () => ({
  type: actionTypes.DELETING_ADDITION_FAIL,
});

export {
  addingAdditionStart,
  addingAdditionFail,
  addingAdditionSuccess,
  editingAdditionStart,
  editingAdditionFail,
  editingAdditionSuccess,
  receivingAdditionsStart,
  receivingAdditionsSuccess,
  receivingAdditionsFail,
  deletingAdditonStart,
  deletingAdditionSuccess,
  deletingAdditionFail,
};
