// @flow
import {
 put, call, all, takeEvery,
} from 'redux-saga/effects';
import * as service from './service';
import * as actions from './actions';
import * as actionTypes from './actionTypes';

function* addAddition(action) {
  try {
    yield put(actions.addingAdditionStart());
    const data = yield call(service.addAddition, action.data);
    yield put(actions.addingAdditionSuccess(data));
  } catch (err) {
    yield put(actions.addingAdditionFail(err));
  }
}

function* editAddition(action) {
  try {
    yield put(actions.editingAdditionStart());
    const data = yield call(service.editAddition, action.id, action.data);
    yield put(actions.editingAdditionSuccess(data));
  } catch (err) {
    yield call(actions.editingAdditionFail(err));
  }
}

function* receiveAdditions(action) {
  try {
    yield put(actions.receivingAdditionsStart());
    const data = yield call(service.receiveAdditions, action.id);
    yield put(actions.receivingAdditionsSuccess(data));
  } catch (err) {
    yield put(actions.receivingAdditionsFail(err));
  }
}

function* deleteAddition(action) {
  try {
    yield put(actions.deletingAdditonStart());
    yield call(service.deleteAddition, action.id);
    yield put(actions.deletingAdditionSuccess(action.id));
  } catch (err) {
    yield put(actions.deletingAdditionFail());
  }
}

export default function* additionSaga() {
  yield all([
    yield takeEvery(actionTypes.ADDING_ADDITION, addAddition),
    yield takeEvery(actionTypes.EDITING_ADDITION, editAddition),
    yield takeEvery(actionTypes.RECEIVING_ADDITIONS, receiveAdditions),
    yield takeEvery(actionTypes.DELETING_ADDITION, deleteAddition),
  ]);
}
