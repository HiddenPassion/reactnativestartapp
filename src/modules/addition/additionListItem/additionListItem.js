// @flow
import React from 'react';
import { View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Input from '../common/input';
import Button from '../common/button';
import commonStyles from '../common/styles';

type Props = {
  isEditable: boolean,
  isDisabled: boolean,
  name: string,
  price: string,
  onNameChange: Function,
  onPriceChange: Function,
  onDisableButtonClick: Function,
  onSaveEditButtonClick: Function,
  onCancelButtonClick: Function,
  onEditButtonClick: Function,
  onDeleteButtonClick: Function,
};

const AdditionListItem = ({
  name,
  price,
  isEditable,
  isDisabled,
  onSaveEditButtonClick,
  onCancelButtonClick,
  onEditButtonClick,
  onDeleteButtonClick,
  onDisableButtonClick,
  onNameChange,
  onPriceChange,
}: Props) => (
  <View style={commonStyles.createContainer}>
    <Input onChangeText={onNameChange} value={name} editable={isEditable} />
    <Input onChangeText={onPriceChange} value={price} editable={isEditable} />
    {isEditable ? (
      <React.Fragment>
        <Button onPress={onSaveEditButtonClick} name="md-folder-open" size={30} />
        <Button onPress={onCancelButtonClick} name="md-close" size={30} />
      </React.Fragment>
    ) : (
      <React.Fragment>
        <Button onPress={onEditButtonClick} name="md-create" />
        <Button onPress={onDeleteButtonClick} name="md-trash" size={30} />
        <Button
          onPress={onDisableButtonClick}
          name={isDisabled ? 'md-eye-off' : 'md-eye'}
          size={30}
        />
      </React.Fragment>
    )}
  </View>
);

export default AdditionListItem;
