// @flow
import { connect } from 'react-redux';
import { Alert } from 'react-native';
import {
  compose,
  withStateHandlers,
  onlyUpdateForKeys,
  lifecycle,
  withState,
  withHandlers,
} from 'recompose';

import * as actionTypes from '../actionTypes';
import AdditionListItem from './additionListItem';

const mapDispatchToProps = dispatch => ({
  deleteAddition: id => dispatch({ type: actionTypes.DELETING_ADDITION, id }),
  editAddition: (id, data) => dispatch({ type: actionTypes.EDITING_ADDITION, id, data }),
});

export default compose(
  onlyUpdateForKeys(['name', 'price', 'isDisabled']),
  connect(
    null,
    mapDispatchToProps,
  ),
  withState('name', 'changeName', ({ name }) => name),
  withState('price', 'changePrice', ({ price }) => price),
  withState('isDisabled', 'changeDisableStatus', ({ isDisabled }) => isDisabled),
  withState('isEditable', 'changeEditableStatus', false),
  withState('initialValue', '', ({ name, price }) => ({
    name,
    price,
  })),
  withHandlers({
    onNameChange: ({ changeName }) => value => changeName(value),
    onPriceChange: ({ changePrice }) => value => changePrice(value),
    onEditButtonClick: ({ changeEditableStatus }) => () => changeEditableStatus(true),
    onSaveEditButtonClick: ({
 id, name, price, editAddition, changeEditableStatus,
}) => () => {
      editAddition(id, { name, price });
      changeEditableStatus(false);
    },
    onCancelButtonClick: ({
      changeName,
      changePrice,
      changeEditableStatus,
      initialValue,
    }) => () => {
      changeName(initialValue.name);
      changePrice(initialValue.price);
      changeEditableStatus(false);
    },
    onDisableButtonClick: ({
 id, isDisabled, editAddition, changeDisableStatus,
}) => () => {
      editAddition(id, { isDisabled: !isDisabled });

      changeDisableStatus(!isDisabled);
    },
    onDeleteButtonClick: ({ id, deleteAddition }) => () => {
      Alert.alert('You really wanna delete ?', '', [
        { text: 'Yes', onPress: () => deleteAddition(id) },
        { text: 'No' },
      ]);
    },
  }),
)(AdditionListItem);
