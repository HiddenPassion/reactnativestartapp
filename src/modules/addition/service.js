// @flow
import axios from '../../utility/axios';
import * as requestPath from './utility/RequestUrlPath';

const addAddition = (...data: Array<string>) => axios.post(requestPath.ADDITION, ...data);
const editAddition = (id: string, ...data: Array<string>) => axios.patch(`${requestPath.ADDITION}/${id}`, ...data);
const receiveAdditions = (id: string) => axios.get(`${requestPath.ADDITION}/${id}`);
const deleteAddition = (id: string) => axios.delete(`${requestPath.ADDITION}/${id}`);

export {
 addAddition, editAddition, receiveAdditions, deleteAddition,
};
