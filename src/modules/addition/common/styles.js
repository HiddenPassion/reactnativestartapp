// @flow
import React from 'react';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  createContainer: {
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});
