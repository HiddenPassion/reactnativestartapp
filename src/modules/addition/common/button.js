// @flow
import React from 'react';
import { TouchableOpacity, View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const styles = StyleSheet.create({
  handlerContainer: {
    width: '8%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

type Props = {
  onPress: Function,
  name: string,
};

const button = ({ onPress, name }: Props) => (
  <TouchableOpacity onPress={onPress} style={styles.handlerContainer}>
    <View>
      <Icon name={name.toString()} size={30} />
    </View>
  </TouchableOpacity>
  );

export default button;
