// @flow
import React from 'react';
import { View, TextInput, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  inputContainer: {
    width: '38%',
  },
});

type Props = {
  placeholder?: string,
  value: string,
  onChangeText: Function,
  editable?: boolean,
};

const input = (props: Props) => (
  <View style={styles.inputContainer}>
    <TextInput {...props} />
  </View>
);

export default input;
