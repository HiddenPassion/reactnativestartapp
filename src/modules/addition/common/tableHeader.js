// @flow
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  header: {
    alignItems: 'flex-start',
    width: '38%',
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 22,
  },
});

type Props = {
  text: string,
};

const tableHeader = ({ text }: Props) => (
  <View style={styles.header}>
    <Text style={styles.headerText}>Name</Text>
  </View>
);

export default tableHeader;
