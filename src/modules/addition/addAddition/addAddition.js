// @flow
import React from 'react';
import {
 View, Text, TextInput, StyleSheet,
} from 'react-native';

import Input from '../common/input';
import Button from '../common/button';
import commonStyles from '../common/styles';

const styles = StyleSheet.create({
  addButton: {
    paddingVertical: 6,
  },
  addButtonText: {
    fontWeight: '500',
    fontSize: 18,
  },
});

type Props = {
  isCreatable: boolean,
  name: string,
  price: string,
  onNameChange: Function,
  onPriceChange: Function,
  onConfirmButtonClick: Function,
  onCancelButtonClick: Function,
  onAddButtonClick: Function,
};

const addAddition = ({
  isCreatable,
  name,
  price,
  onNameChange,
  onPriceChange,
  onConfirmButtonClick,
  onCancelButtonClick,
  onAddButtonClick,
}: Props) => (
  <View style={commonStyles.createContainer}>
    {isCreatable ? (
      <React.Fragment>
        <Input placeholder="name" value={name} onChangeText={onNameChange} />
        <Input placeholder="price" value={price} onChangeText={onPriceChange} />
        <Button onPress={onConfirmButtonClick} name="md-folder-open" />
        <Button onPress={onCancelButtonClick} name="md-close" />
      </React.Fragment>
    ) : (
      <View style={styles.addButton}>
        <Text style={styles.addButtonText} onPress={onAddButtonClick}>
          {'Add new'}
        </Text>
      </View>
    )}
  </View>
);

export default addAddition;
