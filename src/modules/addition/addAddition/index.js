// @flow
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
 compose, withState, withHandlers, onlyUpdateForKeys,
} from 'recompose';

import * as actionTypes from '../actionTypes';
import AddAddition from './addAddition';

const mapDispatchToProps = dispatch => ({
  addAddition: data => dispatch({ type: actionTypes.ADDING_ADDITION, data }),
});

export default compose(
  connect(
    null,
    mapDispatchToProps,
  ),
  onlyUpdateForKeys(['name', 'price', 'isCreatable']),
  withState('name', 'changeName', ''),
  withState('price', 'changePrice', ''),
  withState('isCreatable', 'changeCreatableStatus', false),
  withHandlers({
    onNameChange: ({ changeName }) => value => changeName(value),
    onPriceChange: ({ changePrice }) => value => changePrice(value),
    onAddButtonClick: ({ changeCreatableStatus, isCreatable }) => () => changeCreatableStatus(!isCreatable),
    onConfirmButtonClick: ({
      name,
      price,
      changeName,
      changePrice,
      changeCreatableStatus,
      cinemaId,
      addAddition,
    }) => () => {
      if (name && price) {
        addAddition({
          name,
          price,
          cinema_id: cinemaId,
        });

        changeName('');
        changePrice('');
        changeCreatableStatus(false);
      }
    },
    onCancelButtonClick: ({ changeName, changePrice, changeCreatableStatus }) => () => {
      changeName('');
      changePrice('');
      changeCreatableStatus(false);
    },
  }),
)(AddAddition);
