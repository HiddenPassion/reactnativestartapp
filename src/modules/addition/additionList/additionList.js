// @flow
import React from 'react';
import { FlatList } from 'react-native';
import AdditionHeader from './additionHeader';
import AdditionListItem from '../additionListItem';
import AddAdditionContainer from '../addAddition';

type Props = {
  cinemaId: string,
  additionsList: Array<Object>,
};

const AdditionList = ({ cinemaId, additionsList }: Props) => (
  <FlatList
    ListHeaderComponent={AdditionHeader}
    ListFooterComponent={<AddAdditionContainer cinemaId={cinemaId} />}
    data={additionsList}
    keyExtractor={item => item.id}
    renderItem={({ item }) => (
      <AdditionListItem
        key={item.id}
        name={item.name}
        price={item.price}
        isDisabled={item.isDisabled}
        id={item.id}
      />
    )}
  />
);

export default AdditionList;
