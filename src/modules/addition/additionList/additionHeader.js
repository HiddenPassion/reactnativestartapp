// @flow
import React from 'react';
import { View, StyleSheet } from 'react-native';
import TableHeader from '../common/tableHeader';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    height: 30,
    borderBottomWidth: 1,
  },
  emptySpace: {
    width: '24%',
  },
});

const AdditionHeader = () => (
  <View style={styles.container}>
    <TableHeader text="Name" />
    <TableHeader text="Price" />
    <View style={styles.emptySpace} />
  </View>
);

export default AdditionHeader;
