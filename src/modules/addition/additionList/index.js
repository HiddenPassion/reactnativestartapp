// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import isEqual from 'lodash/isEqual';
import { FlatList } from 'react-native';
import {
 compose, withState, onlyUpdateForKeys, lifecycle,
} from 'recompose';

import * as actionTypes from '../actionTypes';
import * as selector from '../selector';
import AdditionList from './additionList';

const mapStateToProps = state => ({
  isLoading: selector.getLoadingStatus(state),
  additionList: selector.getData(state),
  error: selector.getErrorMessage(state),
});

const mapDispatchToProps = dispatch => ({
  getData: id => dispatch({ type: actionTypes.RECEIVING_ADDITIONS, id }),
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  onlyUpdateForKeys(['additionsList', 'additionList']),
  withState('additionsList', '', []),
  lifecycle({
    componentDidMount() {
      const { cinemaId, getData, navigator } = this.props;

      if (cinemaId) {
        getData(cinemaId);
      } else {
        navigator.pop();
      }
    },
    componentWillReceiveProps(nextProps) {
      const { additionsList } = this.props;
      const { additionList } = nextProps;

      this.setState({ additionsList: additionList });
    },
  }),
)(AdditionList);
