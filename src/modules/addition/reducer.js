// @flow
import cloneDeep from 'lodash/cloneDeep';
import * as actionTypes from './actionTypes';
import mergeObject from '../../utility/mergeObjects';
import initialState from './initialState';

const addingAdditionStart = state => mergeObject(state, {
    error: null,
    isLoading: true,
  });

const addingAdditionSuccess = (state, action) => {
  const copyData = cloneDeep(state.data);
  copyData.push({
    id: action.data.id,
    name: action.data.name,
    price: action.data.price,
    isDisabled: action.data.isDisabled,
  });

  return mergeObject(state, {
    error: null,
    isLoading: false,
    data: copyData,
  });
};

const addingAdditionFail = (state, action) => mergeObject(state, {
    error: action.error,
    isLoading: false,
  });

const editingAdditionStart = state => mergeObject(state, {
    error: null,
  });

const editingAdditionSuccess = (state, action) => {
  const copyData = cloneDeep(state.data);
  for (const key in state.data) {
    if (action.data.id === state.data[key].id) {
      copyData[key] = action.data;
      break;
    }
  }

  return mergeObject(state, {
    data: copyData,
    error: null,
    isLoading: false,
  });
};

const editingAdditionFail = (state, action) => mergeObject(state, {
    error: action.error,
  });

const receivingAdditionsStart = state => mergeObject(state, {
    error: null,
    isLoading: true,
    data: [],
  });

const receivingAdditionsSuccess = (state, action) => mergeObject(state, {
    error: null,
    isLoading: false,
    data: action.data,
  });

const receivingAdditionsFail = (state, action) => mergeObject(state, {
    error: action.error,
    isLoading: false,
  });

const deletingAdditionStart = state => mergeObject(state, {});

const deletingAdditionSuccess = (state, action) => {
  const copyData = cloneDeep(state.data);
  for (const key in state.data) {
    if (action.id === state.data[key].id) {
      copyData.splice(key, 1);
      break;
    }
  }

  return mergeObject(state, {
    data: copyData,
  });
};

const deletingAdditionFail = (state, action) => mergeObject(state, {
    error: action.error,
  });

const reducer = (state: Object = initialState, action: Object) => {
  switch (action.type) {
    case actionTypes.ADDING_ADDITION_START:
      return addingAdditionStart(state);
    case actionTypes.ADDING_ADDITION_SUCCESS:
      return addingAdditionSuccess(state, action);
    case actionTypes.ADDING_ADDITION_FAIL:
      return addingAdditionFail(state, action);
    case actionTypes.EDITING_ADDITION_START:
      return editingAdditionStart(state);
    case actionTypes.EDITING_ADDITION_SUCCESS:
      return editingAdditionSuccess(state, action);
    case actionTypes.EDITING_ADDITION_FAIL:
      return editingAdditionFail(state, action);
    case actionTypes.RECEIVING_ADDITIONS_START:
      return receivingAdditionsStart(state);
    case actionTypes.RECEIVING_ADDITIONS_SUCCESS:
      return receivingAdditionsSuccess(state, action);
    case actionTypes.RECEIVING_ADDITIONS_FAIL:
      return receivingAdditionsFail(state, action);
    case actionTypes.DELETING_ADDITION_START:
      return deletingAdditionStart(state);
    case actionTypes.DELETING_ADDITION_SUCCESS:
      return deletingAdditionSuccess(state, action);
    case actionTypes.DELETING_ADDITION_FAIL:
      return deletingAdditionFail(state, action);
    default:
      return state;
  }
};

export default reducer;
