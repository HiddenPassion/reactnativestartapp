// @flow
import { createSelector } from 'reselect';

const getState = state => state.addition;

const getLoadingStatus = createSelector(getState, state => state.isLoading);
const getErrorMessage = createSelector(getState, state => state.error);
const getData = createSelector(getState, state => state.data);

export { getLoadingStatus, getErrorMessage, getData };
