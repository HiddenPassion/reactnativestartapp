// @flow
import { createSelector } from 'reselect';

const getMoviesState = state => state.movies;

const getAsignedMovies = createSelector(getMoviesState, state => state.moviesList);
const getFreeMovies = createSelector(getMoviesState, state => state.freeMovies);

export { getFreeMovies, getAsignedMovies };
