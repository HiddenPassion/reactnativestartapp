// @flow
import React from 'react';
import {
 Image, View, StyleSheet, TouchableHighlight,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

type Movie = {
  name: string,
  description: string,
  poster: string,
  actors: string,
  length: number,
  yearRelease: number,
};

type Props = {
  movie: Movie,
  onDeleteButtonPress: Function,
};

const asignedMoviesItem = (props: Props) => {
  const { movie, onDeleteButtonPress } = props;
  return (
    <View style={styles.container}>
      <Image resizeMode="stretch" source={{ uri: movie.poster }} style={styles.image} />
      <TouchableHighlight onPress={onDeleteButtonPress} style={styles.deleteButton}>
        <Icon name="md-close" size={30} />
      </TouchableHighlight>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'red',
    position: 'relative',
  },
  image: {
    width: 200,
    height: 200,
  },
  deleteButton: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    position: 'absolute',
    backgroundColor: 'red',
    left: 170,
    right: 0,
    bottom: 0,
    top: 0,
  },
});

export default asignedMoviesItem;
