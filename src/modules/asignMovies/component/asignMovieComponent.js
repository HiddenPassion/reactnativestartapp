// @flow
import React from 'react';
import {
 FlatList, StyleSheet, View, Text,
} from 'react-native';
import AsignedMoviesItemComponent from './asignedMoviesItem';

type Movie = {
  name: string,
  description: string,
  poster: string,
  actors: string,
  length: number,
  yearRelease: number,
};

type Props = {
  movies: Array<Movie>,
  onDeleteButtonPress: Function,
};

const Header = () => (
  <View style={styles.headerContainer}>
    <Text style={styles.headerText}>On Screens Today</Text>
  </View>
);

const asignMovie = (props: Props) => {
  const { movies, onDeleteButtonPress } = props;
  return (
    <React.Fragment>
      <Header />
      <FlatList
        style={styles.container}
        data={movies}
        keyExtractor={item => item.id}
        horizontal
        renderItem={({ item }) => (
          <AsignedMoviesItemComponent
            movie={item}
            onDeleteButtonPress={() => onDeleteButtonPress(item.id)}
          />
        )}
      />
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    // flexWrap: 'wrap',
  },
  headerContainer: {
    alignItems: 'flex-start',
    width: '100%',
    marginTop: 35,
    marginLeft: 20,
  },
  headerText: {
    fontWeight: '600',
    fontSize: 22,
  },
});

export default asignMovie;
