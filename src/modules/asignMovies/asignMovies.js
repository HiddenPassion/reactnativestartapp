// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Alert } from 'react-native';
import * as actions from '../movieList/thunk';
import AsignMoviesComponent from './component/asignMovieComponent';
import * as selector from './selector';

type Props = {
  freeMovies: Array<Object>,
  getAsignedMovies: Function,
  getFreeMovies: Function,
  cinemaId: string,
};

class AsignMovies extends Component<Props> {
  componentWillMount() {
    const { cinemaId, getAsignedMovies, getFreeMovies } = this.props;

    if (cinemaId) {
      getAsignedMovies({ cinemaId, isArchived: false });
      getFreeMovies({ cinemaId, free: true });
    }
  }

  onDeleteButtonPress = (movieId) => {
    Alert.alert('Choice action', '', [
      { text: 'To Archive', onPress: () => this.changeMovieStatus(movieId) },
      { text: 'Delete', onPress: () => this.removeFromAssigned(movieId) },
      { text: 'Back' },
    ]);
  };

  changeMovieStatus = (movieId) => {
    const { changeMovieStatus, cinemaId } = this.props;
    changeMovieStatus({ cinemaId, movieId, isArchived: true });
  };

  removeFromAssigned = (movieId) => {
    const { removeFromAssigned, cinemaId } = this.props;
    removeFromAssigned({ relId: movieId, cinemaId });
  };

  render() {
    const { todayMovies } = this.props;
    return (
      <AsignMoviesComponent movies={todayMovies} onDeleteButtonPress={this.onDeleteButtonPress} />
    );
  }
}

const mapStateToProps = state => ({
  freeMovies: selector.getFreeMovies(state),
  todayMovies: selector.getAsignedMovies(state),
});

const mapDispatchToProps = dispatch => ({
  getAsignedMovies: cinemaId => dispatch(actions.getMoviesWithFilter(cinemaId)),
  getFreeMovies: params => dispatch(actions.fetchFreeMovies(params)),
  changeMovieStatus: params => dispatch(actions.changeMovieStatus(params)),
  removeFromAssigned: params => dispatch(actions.removeFromAssigned(params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AsignMovies);
