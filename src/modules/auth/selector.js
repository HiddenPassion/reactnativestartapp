// @flow
import { createSelector } from 'reselect';

const authState = state => state.auth;

const getAuthStatus = createSelector(authState, state => state.isAuth);
const getLoadingStatus = createSelector(authState, state => state.isLoading);
const getErrorMessage = createSelector(authState, state => state.error);

export { getAuthStatus, getLoadingStatus, getErrorMessage };
