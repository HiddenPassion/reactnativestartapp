// @flow
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import AuthForm from '../commonComponent/authForm/authForm';
import * as selector from '../selector';
import * as actions from '../thunk';
import * as constants from '../utility/constants';
import LoginInputCreator from '../commonComponent/loginInputCreator/loginInputCreator';
import validate from '../utility/validate';
import RegScreen from '../../../navigation/reg';

const controlsTemplate = {
  value: '',
  error: '',
  touched: false,
};

type Props = {
  isLoading: ?boolean,
  error: ?string,
  login: Function,
};

class Login extends Component {
  static navigatorStyle = {
    navBarHidden: true,
  };

  state = {
    email: { ...controlsTemplate },
    password: { ...controlsTemplate },
  };

  inputChangeHandler = (controlName: string, value: string) => {
    this.setState(state => ({
      [controlName]: {
        ...state[controlName],
        value,
        error: state[controlName].touched
          ? validate(controlName, value)
          : state[controlName].touched,
      },
    }));
  };

  onSubmit = () => {
    const { email, password } = this.state;

    if (!email.error && !password.error && email.value && password.value) {
      // eslint-disable-next-line
      this.props.login(email.value, password.value);
    }
  };

  onBlur = (controlName: string) => {
    this.setState(state => ({
      [controlName]: {
        ...state[controlName],
        touched: true,
        error: validate(controlName, state[controlName].value),
      },
    }));
  };

  render() {
    const form = (
      <LoginInputCreator
        state={this.state}
        onChange={this.inputChangeHandler}
        onSubmit={this.onSubmit}
        onBlurHandler={this.onBlur}
      />
    );

    const { email, password } = this.state;
    const { isLoading, error } = this.props;
    const isDisabled = !!(email.error || password.error || !email.value || !password.value);

    return (
      <AuthForm
        isLoading={isLoading}
        form={form}
        type={constants.SIGN_IN}
        errorMessage={error}
        submitHandler={this.onSubmit}
        disabled={isDisabled}
        redirectHandler={RegScreen}
      />
    );
  }
}

Login.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  error: PropTypes.any,
  login: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  isLoading: selector.getLoadingStatus(state),
  error: selector.getErrorMessage(state),
});

const mapDispatchToProps = dispatch => ({
  login: (email, password) => dispatch(actions.signIn(email, password)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
