// @flow
import axios from '../../utility/axios';
import * as constants from './utility/urlConstants';

const signIn = (data: {}) => axios.post(constants.REQUEST_SIGNIN_URL, data);

const signUp = (data: {}) => axios.post(constants.REQUEST_SIGNUP_URL, data);

export { signIn, signUp };
