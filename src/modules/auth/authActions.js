// @flow
import * as actionTypes from './actionTypes';

const authStart = () => ({
  type: actionTypes.AUTH_START,
});

const authSuccess = (token: string, user: string) => ({
  type: actionTypes.AUTH_SUCCESS,
  token,
  user,
});

const authFail = (error: string) => ({
  type: actionTypes.AUTH_FAIL,
  error,
});

const logout = () => ({
  type: actionTypes.AUTH_LOGOUT,
});

export {
 authStart, authSuccess, authFail, logout,
};
