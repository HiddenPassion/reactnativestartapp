// @flow
import * as actionTypes from './actionTypes';
import mergeObjects from '../../utility/mergeObjects';
import initialState from './initialState';

const reducer = (
  state: {} = initialState,
  action: { type: string, token?: string, user?: string, error: string },
) => {
  switch (action.type) {
    case actionTypes.AUTH_START:
      return authStart(state);
    case actionTypes.AUTH_SUCCESS:
      return authSuccess(state, action);
    case actionTypes.AUTH_FAIL:
      return authFail(state, action);
    case actionTypes.AUTH_LOGOUT:
      return authLogout(state);
    default:
      return state;
  }
};

const authStart = state => mergeObjects(state, { error: null, isLoading: true });

const authSuccess = (state, action) => mergeObjects(state, {
    token: action.token,
    user: action.user,
    error: null,
    isLoading: false,
    isAuth: true,
  });

const authFail = (state, action) => mergeObjects(state, {
    error: action.error,
    isLoading: false,
  });

const authLogout = state => mergeObjects(state, {
    token: null,
    user: null,
    isAuth: false,
  });

export default reducer;
