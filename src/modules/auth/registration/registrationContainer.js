// @flow
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import AuthForm from '../commonComponent/authForm/authForm';
import * as selector from '../selector';
import * as actions from '../thunk';
import * as constants from '../utility/constants';
import LoginInputCreator from '../commonComponent/regInputCreator/regInputCreator';
import validate from '../utility/validate';
import LoginScreen from '../../../navigation/navigation';

const controlsTemplate = {
  value: '',
  error: '',
  touched: false,
};

type Props = {
  isLoading: ?boolean,
  error: ?string,
  register: Function,
};

class Login extends Component<Props> {
  static navigatorStyle = {
    navBarHidden: true,
  };

  state = {
    email: { ...controlsTemplate },
    password: { ...controlsTemplate },
    username: { ...controlsTemplate },
    firstname: { ...controlsTemplate },
    lastname: { ...controlsTemplate },
  };

  inputChangeHandler = (controlName, value) => {
    this.setState(state => ({
      [controlName]: {
        ...state[controlName],
        value,
        error: state[controlName].touched
          ? validate(controlName, value)
          : state[controlName].touched,
      },
    }));
  };

  onSubmit = () => {
    const {
 email, password, firstname, lastname, username,
} = this.state;

    if (
      !(
        !email.value
        || !password.value
        || !firstname.value
        || !lastname.value
        || !username.value
        || email.error
        || password.error
        || firstname.error
        || lastname.error
        || username.error
      )
    ) {
      // eslint-disable-next-line
      this.props.register(
        email.value,
        password.value,
        firstname.value,
        lastname.value,
        username.value,
      );
    }
  };

  onBlur = (controlName) => {
    this.setState(state => ({
      [controlName]: {
        ...state[controlName],
        touched: true,
        error: validate(controlName, state[controlName].value),
      },
    }));
  };

  render() {
    const {
 email, password, firstname, lastname, username,
} = this.state;

    const { isLoading, error } = this.props;

    const isDisabled = !!(
      !email.value
      || !password.value
      || !firstname.value
      || !lastname.value
      || !username.value
      || email.error
      || password.error
      || firstname.error
      || lastname.error
      || username.error
    );

    const form = (
      <LoginInputCreator
        state={this.state}
        onChange={this.inputChangeHandler}
        onSubmit={this.onSubmit}
        onBlurHandler={this.onBlur}
      />
    );

    return (
      <AuthForm
        isLoading={isLoading}
        form={form}
        type={constants.SIGN_UP}
        errorMessage={error}
        submitHandler={this.onSubmit}
        disabled={isDisabled}
        redirectHandler={LoginScreen}
      />
    );
  }
}

Login.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  error: PropTypes.any,
  register: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  isLoading: selector.getLoadingStatus(state),
  error: selector.getErrorMessage(state),
});

const mapDispatchToProps = dispatch => ({
  register: (email, password, firstName, lastName, username) =>
    // eslint-disable-next-line
    dispatch(actions.signUp(email, password, firstName, lastName, username)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
