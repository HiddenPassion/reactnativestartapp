// @flow
export const REQUEST_SIGNIN_URL = 'auth/login';
export const SIGNIN_URL = '/login';
export const REQUEST_SIGNUP_URL = 'auth/reg';
export const SIGNUP_URL = '/signup';
