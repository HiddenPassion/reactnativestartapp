// @flow
const validateEmail = val =>
  // eslint-disable-next-line
  /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
    val,
  );

export default (controlName: string, value: string) => {
  if (!value) {
    return ' is required';
  }

  switch (controlName) {
    case 'email': {
      if (validateEmail(value)) {
        return '';
      }

      return ' is not valid';
    }
    case 'password': {
      if (value.length < 6) {
        return ' must have more than 6 characters';
      }
      return '';
    }
    default:
      return '';
  }
};
