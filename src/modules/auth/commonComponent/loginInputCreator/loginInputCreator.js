import React from 'react';
import PropTypes from 'prop-types';
import Input from '../input/input';


/* eslint-disable react/no-this-in-sfc, no-return-assign */

// eslint-disable-next-line
const loginInputCreator = ({state, onBlurHandler, onChange, onSubmit }) => (
    <React.Fragment>
        <Input
            value={state.email.value}
            type="email"
            errorMessage={state.email.error}
            returnKeyType="next"
            onSubmitEditing={() => this.passwordTextInput.focus()}
            blurOnSubmit={false}
            onBlur={() => onBlurHandler('email')}
            keyboardType="email-address"
            underlineColorAndroid={state.email.error ? 'red' : 'blue'}
            onChangeText={e => onChange('email', e)}
        />
        <Input
            value={state.password.value}
            type="password"
            errorMessage={state.password.error}
            returnKeyType="done"
            secureTextEntry
            setRef={input => this.passwordTextInput = input}
            onBlur={() => onBlurHandler('password')}
            underlineColorAndroid={state.password.error ? 'red' : 'blue'}
            onSubmitEditing={onSubmit}
            onChangeText={e => onChange('password', e)}
        />
    </React.Fragment>
);

loginInputCreator.propTypes = {
    state: PropTypes.object.isRequired,
    onBlurHandler: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
};

export default loginInputCreator;
