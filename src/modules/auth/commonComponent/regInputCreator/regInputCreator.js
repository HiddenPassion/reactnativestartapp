// @flow
import React from 'react';
import PropTypes from 'prop-types';
import Input from '../input/input';
/* eslint-disable react/no-this-in-sfc, no-return-assign */

// eslint-disable-next-line
const regInputCreator = ({ state, onBlurHandler, onChange, onSubmit }) => (
  <React.Fragment>
    <Input
      value={state.email.value}
      type="email"
      errorMessage={state.email.error}
      returnKeyType="next"
      onSubmitEditing={() => this.usernameTextInput.focus()}
      blurOnSubmit={false}
      onBlur={() => onBlurHandler('email')}
      keyboardType="email-address"
      underlineColorAndroid={state.email.error ? 'red' : 'blue'}
      onChangeText={e => onChange('email', e)}
    />
    <Input
      value={state.username.value}
      type="username"
      errorMessage={state.username.error}
      returnKeyType="next"
      setRef={input => (this.usernameTextInput = input)}
      onSubmitEditing={() => this.firstnameTextInput.focus()}
      blurOnSubmit={false}
      onBlur={() => onBlurHandler('username')}
      underlineColorAndroid={state.username.error ? 'red' : 'blue'}
      onChangeText={e => onChange('username', e)}
    />
    <Input
      value={state.firstname.value}
      type="firstname"
      errorMessage={state.firstname.error}
      returnKeyType="next"
      setRef={input => (this.firstnameTextInput = input)}
      onSubmitEditing={() => this.lastnameTextInput.focus()}
      blurOnSubmit={false}
      onBlur={() => onBlurHandler('firstname')}
      underlineColorAndroid={state.firstname.error ? 'red' : 'blue'}
      onChangeText={e => onChange('firstname', e)}
    />
    <Input
      value={state.lastname.value}
      type="lastname"
      errorMessage={state.lastname.error}
      returnKeyType="next"
      setRef={input => (this.lastnameTextInput = input)}
      onSubmitEditing={() => this.passwordTextInput.focus()}
      blurOnSubmit={false}
      onBlur={() => onBlurHandler('lastname')}
      underlineColorAndroid={state.lastname.error ? 'red' : 'blue'}
      onChangeText={e => onChange('lastname', e)}
    />
    <Input
      value={state.password.value}
      type="password"
      errorMessage={state.password.error}
      returnKeyType="done"
      setRef={input => (this.passwordTextInput = input)}
      onBlur={() => onBlurHandler('password')}
      secureTextEntry
      underlineColorAndroid={state.password.error ? 'red' : 'blue'}
      onSubmitEditing={onSubmit}
      onChangeText={e => onChange('password', e)}
    />
  </React.Fragment>
);

regInputCreator.propTypes = {
  state: PropTypes.object.isRequired,
  onBlurHandler: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default regInputCreator;
