// @flow
import React from 'react';
import PropTypes from 'prop-types';

import {
 View, Text, StyleSheet, TouchableOpacity,
} from 'react-native';

type Props = {
  disabled: ?boolean,
  onSubmit: Function,
  type: string,
};

const button = ({ disabled, onSubmit, type }: Props) => {
  const content = (
    <View style={[styles.button, disabled ? styles.disabled : null]}>
      <Text style={[styles.buttonText, disabled ? styles.disabledText : null]}>{type}</Text>
    </View>
  );

  return disabled ? content : <TouchableOpacity onPress={onSubmit}>{content}</TouchableOpacity>;
};

button.propTypes = {
  disabled: PropTypes.bool,
  type: PropTypes.string.isRequired,
  onSubmit: PropTypes.func,
};

const styles = StyleSheet.create({
  button: {
    borderRadius: 5,
    backgroundColor: 'blue',
    width: 150,
    height: 20,
    padding: 20,
    margin: 5,
    borderWidth: 1,
    borderColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
  disabled: {
    backgroundColor: '#eee',
    borderColor: '#aaa',
  },
  disabledText: {
    color: '#aaa',
  },
});

export default button;
