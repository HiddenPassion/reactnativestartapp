// @flow
import PropTypes from 'prop-types';
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  KeyboardAvoidingView,
  ActivityIndicator,
} from 'react-native';
import Button from '../button/button';
import * as constants from '../../utility/constants';

type Props = {
  type: string,
  form: any, // react component
  isLoading: ?boolean,
  submitHandler: Function,
  disabled: boolean,
  redirectHandler: Function,
};

const authForm = ({
 type, form, isLoading, submitHandler, disabled, redirectHandler,
}: Props) => (
  <KeyboardAvoidingView style={styles.container} behavior="padding">
    <View style={styles.iconContainer}>
      <Image
        source={require('../../../../assets/images/CinemaIcon.png')}
        style={[type === constants.SIGN_IN ? styles.iconLoginMode : styles.iconRegMode]}
      />
    </View>
    {form}
    <View style={styles.buttonContainer}>
      {isLoading ? (
        <ActivityIndicator size="small" color="blue" />
      ) : (
        <Button onSubmit={submitHandler} type={type} disabled={disabled} />
      )}
    </View>
    {type === constants.SIGN_IN ? (
      <View style={styles.switchMode}>
        <Text>Don`t have an account? </Text>
        {/* eslint-disable-next-line */}
        <Text style={styles.switchButton} onPress={redirectHandler}>
          Sign Up
        </Text>
      </View>
    ) : (
      <View style={styles.switchMode}>
        <Text>Already have an account? </Text>
        {/* eslint-disable-next-line */}
        <Text style={styles.switchButton} onPress={redirectHandler}>
          Log In
        </Text>
      </View>
    )}
  </KeyboardAvoidingView>
);

authForm.propTypes = {
  disabled: PropTypes.bool.isRequired,
  type: PropTypes.string.isRequired,
  errorMessage: PropTypes.string,
  submitHandler: PropTypes.func.isRequired,
  form: PropTypes.element.isRequired,
  redirectHandler: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    backgroundColor: 'rgb(212, 247, 252)',
  },
  buttonContainer: {
    marginTop: 20,
    marginBottom: 10,
  },
  iconContainer: {
    marginBottom: 50,
  },
  iconLoginMode: {
    width: 250,
    height: 250,
  },
  iconRegMode: {
    width: 100,
    height: 100,
  },
  switchMode: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  switchButton: {
    fontSize: 16,
    fontWeight: 'bold',
    textDecorationLine: 'underline',
    color: 'rgb(196, 41, 170)',
  },
});

export default authForm;
