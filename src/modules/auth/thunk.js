// @flow
import { AsyncStorage, Alert } from 'react-native';
import * as authService from './authService';
import * as actions from './authActions';

import LoginScreen from '../../navigation/login';
import MainContentScreen from '../../navigation/mainContent';

// import { takeEvery, delay, put, call, race} from 'redux-saga/effects';

// function* signIn(email, password) {
//     try {
//         yield put(actions.authStart());
//         const { token, timeout} = yield race({
//             token: call(authService.signIn({email, password})),
//             timeout: call(delay, 10000)
//         });
//         if (token) {
//             AsyncStorage.setItem('token', res.data.token.split(' ')[1]);
//             AsyncStorage.setItem('user', JSON.stringify(res.data.user));
//             yield put(actions.authSuccess(res.data.token, res.data.user));
//         } else {
//             yield put(actions.authFail('Can not connect to the server. Try again!'));
//         }
//     } catch (err) {
//         yield put(actions.authFail(err));
//     }
// }

const signIn = (email: string, password: string) => (dispatch: Function) => {
  dispatch(actions.authStart());

  authService
    .signIn({ email, password })
    .then((res) => {
      AsyncStorage.setItem('token', res.data.token.split(' ')[1]);
      AsyncStorage.setItem('user', JSON.stringify(res.data.user));

      dispatch(actions.authSuccess(res.data.token, res.data.user));

      MainContentScreen();
    })
    .catch((err) => {
      if (err.response.status === 401) {
        dispatch(actions.authFail('Invalid mail or password'));
        Alert.alert('Invalid mail or password');
      } else {
        dispatch(actions.authFail(err.response.data.error));
        Alert.alert('Something went wrong. Try again');
      }
    });
};

const signUp = (
  email: string,
  password: string,
  firstName: string,
  lastName: string,
  username: string,
) => (dispatch: Function) => {
  dispatch(actions.authStart());

  authService
    .signUp({
      email,
      password,
      firstName,
      lastName,
      username,
      isadmin: true,
    })
    .then((res) => {
      AsyncStorage.setItem('token', res.data.token.split(' ')[1]);
      AsyncStorage.setItem('user', JSON.stringify(res.data.user));

      dispatch(actions.authSuccess(res.data.token, res.data.user));
      MainContentScreen();
    })
    .catch((err) => {
      dispatch(actions.authFail(err.response.data.error));
      Alert.alert('Something went wrong. Try again');
    });
};

const authCheckState = () => async (dispatch: Function) => {
  try {
    const token = await AsyncStorage.getItem('token');
    const user = await AsyncStorage.getItem('user');

    if (token && user) {
      dispatch(actions.authSuccess(token, JSON.parse(user)));
      MainContentScreen();
    } else {
      // eslint-disable-next-line
      throw 'no token or user';
    }
  } catch (err) {
    AsyncStorage.removeItem('token');
    AsyncStorage.removeItem('user');

    dispatch(actions.logout());
    LoginScreen();
  }
};

const clearAuthStore = () => () => {
  AsyncStorage.removeItem('token');
  return AsyncStorage.removeItem('user');
};

const logout = () => (dispatch: Function) => {
  dispatch(clearAuthStore())
    .then(() => LoginScreen())
    .then(() => {
      dispatch(actions.logout());
    });
};

export {
 signIn, signUp, authCheckState, logout,
};
