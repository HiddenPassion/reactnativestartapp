// @flow
export default {
  token: null,
  user: null,
  error: null,
  isLoading: false,
  isAuth: undefined,
};
