// @flow
import axios from '../../utility/axios';
import * as requestPath from './utility/requesUrlConstants';

const getOptions = () => axios.get(requestPath.GET_OPTIONS);
const getCinemasWithFilter = () => axios.get(requestPath.GET_DATA_WITH_FILTER);
const getCinemaById = (id: string) => axios.get(requestPath.GET_DATA_WITH_FILTER + id);
const deleteCinema = (id: string) => axios.delete(requestPath.GET_DATA_WITH_FILTER + id);

export {
 getOptions, getCinemasWithFilter, getCinemaById, deleteCinema,
};
