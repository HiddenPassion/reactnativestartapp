// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as selector from '../../selector';
import * as actions from '../../thunk';

import FilteringComponent from '../filteringComponent/filteringComponent';

type Props = {
  getOptions: Function,
  nameList: Array<string>,
  getCinemasLocally: Function,
};

class Filtering extends Component<Props> {
  state = {
    name: '',
  };

  componentWillMount = () => {
    const { getOptions } = this.props;
    getOptions();
  };

  setParams = (data: { name?: string }) => ({
    name: data.name ? data.name : '',
  });

  onSubmit = () => {
    const { getCinemasLocally } = this.props;
    getCinemasLocally(this.setParams(this.state));
  };

  onInputChange = (value) => {
    this.setState({ name: value }, () => {
      this.onSubmit();
    });
  };

  render() {
    const { name } = this.state;
    return (
      <FilteringComponent
        onInputChange={this.onInputChange}
        onSubmit={this.onSubmit}
        inputFieldValue={name}
      />
    );
  }
}

const mapStateToProps = state => ({
  isOptionsLoading: selector.getOptionsLoadingStatus(state),
  optionsError: selector.getOptionsErrorMessage(state),
  nameList: selector.getNameList(state),
  cityList: selector.getCityList(state),
  addressList: selector.getAddressList(state),
});

const mapDispatchToProps = dispatch => ({
  getOptions: data => dispatch(actions.getOptions(data)),
  getCinemas: data => dispatch(actions.getCinemasWithFilter(data)),
  getCinemasLocally: data => dispatch(actions.gettingCinemasWithFilterLocally(data)), //
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Filtering);
