// @flow
import Filtering from './filteringContainer/filteringContainer';
import filteringReducer from '../reducers/cinemaListReducer';

export { Filtering, filteringReducer };
