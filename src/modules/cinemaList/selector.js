// @flow
import { createSelector } from 'reselect';

const getState = (state: { cinemas: {} }) => state.cinemas;

const getOptionsLoadingStatus = createSelector(getState, state => state.isOptionsLoading);
const getOptionsErrorMessage = createSelector(getState, state => state.error);
const getNameList = createSelector(getState, state => state.nameList);
const getCityList = createSelector(getState, state => state.cityList);
const getAddressList = createSelector(getState, state => state.addressList);

export {
  getOptionsLoadingStatus,
  getOptionsErrorMessage,
  getNameList,
  getCityList,
  getAddressList,
};
