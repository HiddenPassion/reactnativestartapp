// @flow
import * as cinemaListService from './cinemaListService';
import * as actions from './actions/cinemaListActions';

const getOptions = () => (dispatch: Function) => {
  dispatch(actions.gettingOptionsStart());

  cinemaListService
    .getOptions()
    .then((res) => {
      dispatch(actions.gettingOptionsSuccess(res));
    })
    .catch((err) => {
      dispatch(actions.gettingOptionsFail(err));
    });
};

const getCinemasWithFilter = () => (dispatch: Function) => {
  dispatch(actions.gettingCinemasWithFilterStart());
  cinemaListService
    .getCinemasWithFilter()
    .then((res) => {
      dispatch(actions.gettingCinemasWithFilterSuccess(res));
    })
    .catch((err) => {
      dispatch(actions.gettingCinemasWithFilterFail(err));
    });
};

const getCinemaById = (filter: string) => (dispatch: Function) => {
  dispatch(actions.gettingCinemasWithFilterStart());

  cinemaListService
    .getCinemaById(filter)
    .then((res) => {
      dispatch(actions.gettingCinemasWithFilterSuccess(res));
    })
    .catch((err) => {
      dispatch(actions.gettingCinemasWithFilterFail(err));
    });
};

const deleteCinema = (id: string) => () => {
  cinemaListService.deleteCinema(id);
  getCinemasWithFilter();
};

const gettingCinemasWithFilterLocally = (filter: { name: string }) => (
  dispatch: Function,
  getState: Function,
) => {
  const res = getState().cinemas.cinemasList.filter(
    ({ name }) => name.toLowerCase().indexOf(filter.name.toLowerCase()) > -1,
  );
  dispatch(actions.gettingCinemasWithFilterLocally(res));
};

export {
  getOptions,
  getCinemasWithFilter,
  getCinemaById,
  deleteCinema,
  gettingCinemasWithFilterLocally,
};
