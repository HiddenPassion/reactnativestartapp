// @flow
import * as actionTypes from './actionTypes';

const gettingOptionsStart = () => ({
  type: actionTypes.GETTING_CINEMA_OPTIONS_START,
});

const gettingOptionsFail = (data: {}) => ({
  type: actionTypes.GETTING_CINEMA_OPTIONS_FAIL,
  error: data,
});

const gettingOptionsSuccess = (data: {}) => ({
  type: actionTypes.GETTING_CINEMA_OPTIONS_SUCCESS,
  data: data.data.options.name,
});

const gettingCinemasWithFilterStart = () => ({
  type: actionTypes.GETTING_CINEMAS_WITH_FILTER_START,
});
const gettingCinemasWithFilterSuccess = (data: {}) => ({
  type: actionTypes.GETTING_CINEMAS_WITH_FILTER_SUCCESS,
  data: data.data.cinemas,
});
const gettingCinemasWithFilterFail = (err: {}) => ({
  type: actionTypes.GETTING_CINEMAS_WITH_FILTER_FAIL,
  error: err,
});

const gettingCinemasWithFilterLocally = (data: Array<{}>) => ({
  type: actionTypes.GETTING_CINEMA_WITH_FILTER_LOCALLY,
  data,
});

export {
  gettingOptionsStart,
  gettingOptionsSuccess,
  gettingOptionsFail,
  gettingCinemasWithFilterStart,
  gettingCinemasWithFilterSuccess,
  gettingCinemasWithFilterFail,
  gettingCinemasWithFilterLocally,
};
