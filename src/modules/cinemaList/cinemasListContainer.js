// @flow
import React from 'react';
import { connect } from 'react-redux';
import {
 lifecycle, compose, onlyUpdateForKeys, withHandlers,
} from 'recompose';
import isEmpty from 'lodash/isEmpty';
import CinemaList from './components/cinemaList';

import * as actions from './thunk';

type CinemaProps = {
  getData: Function,
  onCinemaPosterClick: Function,
  isLoading: ?boolean,
  cinemas: {},
};

const CinemaListContainer = (props: CinemaProps) => {
  const {
 cinemas, isLoading, getData, onCinemaPosterClick,
} = props;
  return (
    <CinemaList
      cinemas={cinemas}
      isLoading={isLoading}
      onRefresh={getData}
      onCinemaPosterClick={onCinemaPosterClick}
    />
  );
};

const mapDispatchToProps = dispatch => ({
  getData: () => dispatch(actions.getCinemasWithFilter()),
});

const mapStateToProps = state => ({
  cinemas: state.cinemas.cinemasListLocally,
  isLoading: state.cinemas.isCinemasLoading,
});

export default compose(
  onlyUpdateForKeys(['cinemas, isLoading']),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withHandlers({
    onNavigatorEvent: props => (event) => {
      if (event.type === 'ScreenChangedEvent' && event.id === 'willAppear') {
        if (isEmpty(props.movies)) {
          props.getData();
        }
      }

      if (event.type === 'NavBarButtonPress' && event.id === 'sideDrawerToggle') {
        props.navigator.toggleDrawer({
          side: 'left',
        });
      }

      if (event.type === 'NavBarButtonPress' && event.id === 'addButton') {
        props.navigator.push({
          screen: 'navigation.cinemaCreationPage',
          title: 'Adding Cinema',
        });
      }
    },
    onCinemaPosterClick: props => (id) => {
      const selectedCinema = props.cinemas.find(cinema => cinema.id === id);

      props.navigator.push({
        screen: 'navigation.cinemaPage',
        title: selectedCinema.name,
        passProps: {
          cinema: selectedCinema,
        },
      });
    },
  }),
  lifecycle({
    componentWillMount() {
      const { navigator } = this.props;

      navigator.setOnNavigatorEvent(this.props.onNavigatorEvent);
    },
  }),
)(CinemaListContainer);
