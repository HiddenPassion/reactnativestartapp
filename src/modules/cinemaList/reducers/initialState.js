// @flow
export default {
  isOptionsLoading: false,
  optionsError: null,
  nameList: [],
  cinemasList: [],
  cinemasListLocally: [],
  isCinemasLoading: false,
  cinemasError: null,
  isAssigningMovie: false,
};
