// @flow
import * as actionTypes from '../actions/actionTypes';
import mergeObject from '../../../utility/mergeObjects';
import initialState from './initialState';

const reducer = (
  state: {} = initialState,
  action: { type: string, data?: any, error?: string },
) => {
  switch (action.type) {
    case actionTypes.GETTING_CINEMA_OPTIONS_START:
      return gettingOptionsStart(state);
    case actionTypes.GETTING_CINEMA_OPTIONS_SUCCESS:
      return gettingOptionsSuccess(state, action);
    case actionTypes.GETTING_CINEMA_OPTIONS_FAIL:
      return gettingOptionsFail(state, action);

    case actionTypes.GETTING_CINEMAS_WITH_FILTER_START:
      return gettingCinemasWithFilterStart(state);
    case actionTypes.GETTING_CINEMAS_WITH_FILTER_SUCCESS:
      return gettingCinemasWithFilterSuccess(state, action);
    case actionTypes.GETTING_CINEMAS_WITH_FILTER_FAIL:
      return gettingCinemasWithFilterFail(state, action);
    case actionTypes.GETTING_CINEMA_WITH_FILTER_LOCALLY:
      return gettingCinemasWithFilterLocally(state, action);
    default:
      return state;
  }
};

const gettingOptionsStart = state => mergeObject(state, {
    optionsError: null,
    isOptionsLoading: true,
    nameList: [],
  });

const gettingOptionsSuccess = (state, action) => mergeObject(state, {
    optionsError: null,
    isOptionsLoading: false,
    nameList: action.data,
  });

const gettingOptionsFail = (state, action) => mergeObject(state, {
    optionsError: action.error,
    isOptionsLoading: false,
    nameList: [],
  });

const gettingCinemasWithFilterStart = state => mergeObject(state, {
    cinemasError: null,
    isCinemasLoading: true,
    cinemasList: [],
    cinemasListLocally: [],
  });

const gettingCinemasWithFilterSuccess = (state, action) => mergeObject(state, {
    cinemasError: null,
    isCinemasLoading: false,
    cinemasList: action.data,
    cinemasListLocally: action.data,
  });

const gettingCinemasWithFilterFail = (state, action) => mergeObject(state, {
    cinemasError: action.error,
    isCinemasLoading: false,
  });

const gettingCinemasWithFilterLocally = (state, action) => mergeObject(state, {
    cinemasError: null,
    cinemasListLocally: action.data,
  });

export default reducer;
