// @flow
export const GET_OPTIONS = '/cinema/options';
export const GET_DATA_WITH_FILTER = '/cinema/';
export const ASSIGN_MOVIE_URL = '/cinema/assignedMovie/';
