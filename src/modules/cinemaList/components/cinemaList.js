// @flow
import React from 'react';

import { View, FlatList, StyleSheet } from 'react-native';
import CinemaListItem from './cinemaListItem';
import { Filtering } from '../filtering';

const separator = () => (
  <View style={styles.separator}>
    <View style={styles.separatorLine} />
  </View>
);

type Props = {
  isLoading: ?boolean,
  cinemas: {},
  onRefresh: Function,
  onCinemaPosterClick: Function,
};

const CinemaList = (props: Props) => {
  const {
 isLoading, onRefresh, onCinemaPosterClick, cinemas,
} = props;
  return (
    <View style={styles.container}>
      <FlatList
        ListHeaderComponent={Filtering}
        refreshing={isLoading}
        onRefresh={onRefresh}
        data={cinemas}
        keyExtractor={item => item.id}
        renderItem={({ item }) => (
          <CinemaListItem cinema={item} onCinemaPosterClick={() => onCinemaPosterClick(item.id)} />
        )}
        style={styles.flatList}
        ItemSeparatorComponent={separator}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eee',
    alignItems: 'center',
  },
  flatList: {
    width: '100%',
  },
  separator: {
    width: '100%',
    backgroundColor: '#eee',
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  separatorLine: {
    height: 1.5,
    width: '94%',
    backgroundColor: '#ccc',
  },
});

export default CinemaList;
