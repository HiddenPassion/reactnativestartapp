// @flow
import React from 'react';
import {
 View, Image, StyleSheet, TouchableWithoutFeedback,
} from 'react-native';

type Props = {
  onCinemaPosterClick: Function,
  cinema: {
    imageUrl: string,
  },
};

const cinemaListItem = ({ onCinemaPosterClick, cinema }: Props) => (
  <TouchableWithoutFeedback onPress={onCinemaPosterClick}>
    <View style={styles.container}>
      <Image source={{ uri: cinema.imageUrl }} style={styles.image} resizeMode="stretch" />
    </View>
  </TouchableWithoutFeedback>
);

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: '90%',
    height: 350,
    borderColor: '#ccc',
    borderWidth: 0.7,
  },
});

export default cinemaListItem;
