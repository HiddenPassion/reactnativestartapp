// @flow
import React from 'react';
import {
 View, Text, Image, TouchableOpacity, StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

type Props = {
  logoutHandler: Function,
  userInfo: {},
};

const movieSidebar = ({ logoutHandler, userInfo }: Props) => (
  <View style={styles.container}>
    <View style={styles.profileContainer}>
      <View style={styles.userInfoContainer}>
        <Image source={require('../../assets/images/UserIcon.png')} style={styles.logo} />
        <View style={styles.userInfo}>
          <Text style={styles.userInfoTextDescription}>{userInfo.lastName}</Text>
          <Text style={styles.userInfoTextDescription}>{` ${userInfo.firstName}`}</Text>
        </View>
      </View>
      <TouchableOpacity onPress={logoutHandler}>
        <Icon style={styles.loginIcon} name="md-log-out" size={40} color="black" />
      </TouchableOpacity>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  profileContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 5,
    paddingVertical: 10,
    borderColor: '#ccc',
    borderWidth: 3,
    backgroundColor: 'rgb(242, 242, 242)',
  },
  userInfoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  logo: {
    marginLeft: 10,
    width: 60,
    height: 60,
    marginRight: 12,
  },
  userInfo: {
    flexDirection: 'row',
  },
  userInfoTextDescription: {
    fontWeight: '400',
    fontSize: 20,
    color: 'black',
  },
  loginIcon: {
    marginRight: 10,
  },
});

export default movieSidebar;
