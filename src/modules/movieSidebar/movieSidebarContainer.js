// @flow
import React, { Component } from 'react';
import { Alert } from 'react-native';
import { connect } from 'react-redux';
import { compose, onlyUpdateForKeys, withHandlers } from 'recompose';
import MovieSidebarComponent from './movieSidebar';
import { logout } from '../auth/thunk';

const emptyString = '               ';
type Props = {
  logoutHandler: Function,
  user: {},
};

const MovieSidebar = (props: Props) => {
  const { user, logoutHandler } = props;
  return <MovieSidebarComponent userInfo={user} logoutHandler={logoutHandler} />;
};

const mapStateToProps = state => ({
  user: state.auth.user,
});

const mapDispatchToProps = dispatch => ({
  logoutHandler: () => dispatch(logout()),
});

export default compose(
  onlyUpdateForKeys(['user']),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withHandlers({
    logoutHandler: (empty, props) => () => {
      const { logoutHandler } = props;

      Alert.alert(`${emptyString}You realy want logout?`, '', [
        // eslint-disable-next-line
        { text: `${emptyString}Yes${emptyString}`, onPress: logoutHandler },
        { text: `${emptyString}No${emptyString}`, style: 'cancel' },
      ]);
    },
  }),
)(MovieSidebar);
