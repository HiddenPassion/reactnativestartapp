// @flow
import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../auth/thunk';

class StartScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
  };

  componentDidMount() {
    const { tryAutoSignin } = this.props;
    // eslint-disable-next-line
    tryAutoSignin();
  }

  render() {
    return (
      <View style={styles.container}>
        <Image source={require('../../assets/images/CinemaIcon.png')} style={styles.logo} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 360,
    height: 360,
  },
});

const mapDispatchToProps = dispatch => ({
  tryAutoSignin: () => dispatch(actions.authCheckState()),
});

export default connect(
  null,
  mapDispatchToProps,
)(StartScreen);
//----------------------------------------------------

// import Animation from './Animation/animation7';

// export default Animation;
