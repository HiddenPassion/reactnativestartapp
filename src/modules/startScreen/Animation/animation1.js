import React from 'react';
import {
 StyleSheet, Text, View, Animated, Image, Easing,
} from 'react-native';
import UserLogo from '../../assets/images/UserIcon.png';

class Animation extends React.Component {
  constructor() {
    super();
    this.spinValue = new Animated.Value(0);
  }

  componentDidMount() {
    this.spin();
  }

  spin() {
    this.spinValue.setValue(0);
    Animated.timing(this.spinValue, {
      toValue: 1,
      duration: 4000,
      easing: Easing.linear,
    }).start(() => this.spin());
  }

  render() {
    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg'],
    });
    const invertedSpin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['360deg', '0deg'],
    });

    const move = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 900],
    });
    return (
      <View style={styles.container}>
        <Animated.Image
          // eslint-disable-next-line
          style={{ width: 250, height: 250, transform: [{ rotate: spin }] }}
          source={UserLogo}
        />
        <Animated.View
          style={{
            width: 100,
            height: 100,
            backgroundColor: 'blue',
            transform: [{ rotate: invertedSpin }],
          }}
        />
        <View style={{ flexDirection: 'row' }}>
          {/* <Animated.View
            style={{
              width: move,
              height: move,
              backgroundColor: 'blue',
            }}
          /> */}
          <Animated.View
            style={{
              width: 200, // move,
              height: 200, // move,
              backgroundColor: 'blue',
              transform: [{ translateX: move }],
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Animation;
