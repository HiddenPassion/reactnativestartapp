// @flow
import React from 'react';
import { View, StyleSheet, Animated } from 'react-native';

// const dimensions = Dimensions.get('window');

// const WIDTH = dimensions.width;
// const HEIGHT = dimensions.height;

// const length = Math.sqrt((WIDTH * HEIGHT) / 500) - 3;

const arr = [];
for (let i = 0; i < 500; i++) {
  arr.push(i);
}

class Animation extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
  };

  constructor() {
    super();
    this.animatedValue = [];
    arr.forEach((value) => {
      this.animatedValue[value] = new Animated.Value(0);
    });
  }

  componentDidMount() {
    this.animate();
  }

  animate() {
    const animations = arr.map(item => Animated.timing(this.animatedValue[item], {
        toValue: 1,
        duration: 2000,
      }));
    Animated.stagger(10, animations).start();
  }

  render() {
    const animations = arr.map((a, i) => (
      <Animated.View key={i} style={[styles.animatedView, { opacity: this.animatedValue[a] }]} />
    ));
    return <View style={styles.container}>{animations}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  animatedView: {
    height: 20,
    width: 20,
    backgroundColor: 'red',
    marginLeft: 3,
    marginTop: 3,
  },
});

export default Animation;
