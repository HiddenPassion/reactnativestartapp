// @flow
import React from 'react';
import {
 Image, View, StyleSheet, Easing, Animated, Text,
} from 'react-native';
import UserIcon from '../../assets/images/UserIcon.png';

class Animation extends React.Component {
  constructor() {
    super();
    this.springValue = new Animated.Value(0.3);
  }

  spring() {
    this.springValue.setValue(0.3);
    Animated.spring(this.springValue, {
      toValue: 1,
      friction: 0.1,
      //   tension: 1,
    }).start();
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={{ marginBottom: 100 }} onPress={this.spring.bind(this)}>

          Spring
        </Text>
        <Animated.Image
          style={{ width: 200, height: 200, transform: [{ scale: this.springValue }] }}
          source={UserIcon}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Animation;
