// @flow
export default (controlName: string, value: string) => {
  if (!value) {
    return ' is required';
  }

  switch (controlName) {
    case 'description': {
      if (value.length < 10) {
        return ' must have more then 10 characters';
      }

      return '';
    }
    case 'yearRelease': {
      if (!Number.isInteger(+value)) {
        return ' must be integer number';
      }

      if (+value < 1900) {
        return ' must be later than 1900';
      }

      return '';
    }
    case 'length': {
      if (!Number.isInteger(+value) && +value > 0) {
        return ' must be integer positive number';
      }

      return '';
    }
    default:
      return '';
  }
};
