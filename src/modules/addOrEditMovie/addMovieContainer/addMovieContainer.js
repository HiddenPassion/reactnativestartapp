// @flow

import React from 'react';
import { connect } from 'react-redux';
import { compose, withStateHandlers, onlyUpdateForKeys } from 'recompose';

import UpsertMovieComponent from '../components/upsertMovieComponent';
import * as selector from '../selector';
import * as actions from '../thunk';
import validate from '../utility/validate';

type Controls = {
  error: ?string,
  value: string | number,
  touched: boolean,
};

type Params = {
  name: Controls,
  description: Controls,
  poster: Controls,
  actors: Controls,
  length: Controls,
  yearRelease: Controls,
  isLoading: boolean,
  error: string,
  onChange: Function,
  onSubmit: Function,
  onBlur: Function,
  addMovie: Function,
};

// type redux

const AddMovie = (props: Params) => {
  const {
    name,
    description,
    poster,
    actors,
    length,
    yearRelease,
    isLoading,
    error,
    onChange,
    onSubmit,
    onBlur,
  } = props;

  const isDisabled: boolean = !(
    name.value
    && !name.error
    && description.value
    && !description.error
    && poster.value
    && !poster.error
    && actors.value
    && !actors.error
    && length.value
    && !length.error
    && yearRelease.value
    && !yearRelease.error
  );

  return (
    <UpsertMovieComponent
      state={{
        name,
        description,
        poster,
        actors,
        length,
        yearRelease,
      }}
      onChange={onChange}
      onBlurHandler={onBlur}
      submitHandler={onSubmit}
      disabled={isDisabled}
      isLoading={isLoading}
      errorMessage={error}
      type="Add"
    />
  );
};

AddMovie.navigatorStyle = {
  tabBarHidden: true,
};

const mapStateToProps = state => ({
  isLoading: selector.getLoadingStatus(state),
  error: selector.getErrorMessage(state),
});

const mapDispatchToProps = dispatch => ({
  addMovie: data => dispatch(actions.addMovie(data)),
});

const stateTemplate = {
  value: '',
  error: '',
  touched: false,
};

export default compose(
  onlyUpdateForKeys([
    'isLoading',
    'error',
    'name',
    'description',
    'poster',
    'actors',
    'length',
    'yearRelease',
  ]),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withStateHandlers(
    {
      name: { ...stateTemplate },
      description: { ...stateTemplate },
      poster: { ...stateTemplate },
      actors: { ...stateTemplate },
      length: { ...stateTemplate },
      yearRelease: { ...stateTemplate },
    },
    {
      onChange: props => (controlName: string, value: string) => ({
        [controlName]: {
          ...props[controlName],
          value,
          error: props[controlName].touched
            ? validate(controlName, value)
            : props[controlName].error,
        },
      }),
      onSubmit: ({
 name, description, poster, actors, length, yearRelease,
}, realProps) => () => {
        if (
          name.value
          && !name.error
          && description.value
          && !description.error
          && poster.value
          && !poster.error
          && actors.value
          && !actors.error
          && length.value
          && !length.error
          && yearRelease.value
          && !yearRelease.error
        ) {
          const dataObj = {
            name: name.value,
            description: description.value,
            poster: poster.value,
            actors: actors.value,
            length: length.value,
            yearRelease: yearRelease.value,
          };

          realProps.addMovie(dataObj).then(() => {
            realProps.navigator.pop();
          });
        }
      },
      onBlur: props => controlName => ({
        [controlName]: {
          ...props[controlName],
          touched: true,
          error: validate(controlName, props[controlName].value),
        },
      }),
    },
  ),
)(AddMovie);
