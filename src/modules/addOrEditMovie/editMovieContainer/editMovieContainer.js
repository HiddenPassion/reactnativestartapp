// @flow
import React from 'react';
import PropTypes from 'prop-types';
import { Alert } from 'react-native';
import { connect } from 'react-redux';
import isEqual from 'lodash/isEqual';
import {
 compose, withStateHandlers, onlyUpdateForKeys, lifecycle, setPropTypes,
} from 'recompose';
import UpsertMovieComponent from '../components/upsertMovieComponent';
import * as selector from '../selector';
import * as actions from '../thunk';
import validate from '../utility/validate';
import { getMoviesWithFilter } from '../../movieList/thunk';

type Props = {
  name: string,
  description: string,
  poster: string,
  actors: string,
  length: number,
  yearRelease: number,
  isLoading: ?boolean,
  error: ?string,
  onChange: Function,
  onBlur: Function,
  onSubmit: Function,
};

const EditMovie = ({
  name,
  description,
  poster,
  actors,
  length,
  yearRelease,
  isLoading,
  error,
  onChange,
  onBlur,
  onSubmit,
}: Props) => {
  const isDisabled = !(
    name.value
    && !name.error
    && description.value
    && !description.error
    && poster.value
    && !poster.error
    && actors.value
    && !actors.error
    && length.value
    && !length.error
    && yearRelease.value
    && !yearRelease.error
  );

  return (
    <UpsertMovieComponent
      state={{
        name,
        description,
        poster,
        actors,
        length,
        yearRelease,
      }}
      onChange={onChange}
      onBlurHandler={onBlur}
      submitHandler={onSubmit}
      disabled={isDisabled}
      isLoading={isLoading}
      errorMessage={error}
      type="Edit"
    />
  );
};

const mapStateToProps = state => ({
  isLoading: selector.getLoadingStatus(state),
  error: selector.getErrorMessage(state),
  data: selector.getData(state),
});

const mapDispatchToProps = dispatch => ({
  editMovie: data => dispatch(actions.editMovie(data)),
  receiveData: data => dispatch(actions.receiveMovie(data)),
  receiveMovies: () => dispatch(getMoviesWithFilter()),
});

const stateTemplate = {
  value: '',
  error: '',
  touched: '',
};

export default compose(
  onlyUpdateForKeys([
    'isLoading',
    'error',
    'editMovie',
    'receiveData',
    'data',
    'name',
    'description',
    'poster',
    'actors',
    'length',
    'yearRelease',
    'isInitial',
  ]),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  setPropTypes({
    isLoading: PropTypes.bool.isRequired,
    error: PropTypes.any,
    editMovie: PropTypes.func.isRequired,
    receiveData: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired,
  }),
  withStateHandlers(
    {
      name: { ...stateTemplate },
      description: { ...stateTemplate },
      poster: { ...stateTemplate },
      actors: { ...stateTemplate },
      length: { ...stateTemplate },
      yearRelease: { ...stateTemplate },
      isInitial: true,
    },
    {
      setData: ({
 name, description, poster, actors, length, yearRelease,
}) => data => ({
        name: {
          ...name,
          value: data.name,
        },
        description: {
          ...description,
          value: data.description,
        },
        poster: {
          ...poster,
          value: data.poster,
        },
        actors: {
          ...actors,
          value: data.actors,
        },
        length: {
          ...length,
          value: data.length,
        },
        yearRelease: {
          ...yearRelease,
          value: data.yearRelease,
        },
        isInitial: false,
      }),

      onChange: props => (controlName, value) => ({
        [controlName]: {
          ...props[controlName],
          value,
          error: props[controlName].touched
            ? validate(controlName, value)
            : props[controlName].error,
        },
      }),

      onSubmit: (
        {
 name, description, poster, actors, length, yearRelease,
},
        {
 movieId, editMovie, receiveMovies, navigator,
},
      ) => () => {
        if (
          name.value
          && !name.error
          && description.value
          && !description.error
          && poster.value
          && !poster.error
          && actors.value
          && !actors.error
          && length.value
          && !length.error
          && yearRelease.value
          && !yearRelease.error
        ) {
          const dataObj = {
            id: movieId,
            name: name.value,
            description: description.value,
            poster: poster.value,
            actors: actors.value,
            length: length.value,
            yearRelease: yearRelease.value,
          };

          editMovie(dataObj).then(() => {
            receiveMovies();
            navigator.pop();
          });
        }
      },

      onBlur: props => controlName => ({
        [controlName]: {
          ...props[controlName],
          touched: true,
          error: validate(controlName, props[controlName].value),
        },
      }),
    },
  ),
  lifecycle({
    componentWillMount() {
      this.props.receiveData(this.props.movieId);
    },
    componentWillReceiveProps({ error, data, isLoading }) {
      if (this.props.isInitial && !isLoading && !error) {
        if (!isEqual(this.props.data, data)) {
          this.props.setData(data);
        }
      } else if (error) {
        this.props.navigator.pop();
        Alert.alert('Something went wrong', error);
      }
    },
  }),
)(EditMovie);
