// @flow
import { createSelector } from 'reselect';

const movieState = state => state.movie;

const getLoadingStatus = createSelector(movieState, state => state.isLoading);
const getErrorMessage = createSelector(movieState, state => state.error);
const getData = createSelector(movieState, state => state.data);

export { getLoadingStatus, getErrorMessage, getData };
