// @flow
import * as actionTypes from './actionTypes';
import mergeObject from '../../utility/mergeObjects';
import initialState from './initialState';

const addingMovieStart = state => mergeObject(state, {
    error: null,
    isLoading: true,
  });

const addingMovieSuccess = state => mergeObject(state, {
    error: null,
    isLoading: false,
  });

const addingMovieFail = (state, action) => mergeObject(state, {
    error: action.error,
    isLoading: false,
  });

const editingMovieStart = state => mergeObject(state, {
    error: null,
    isLoading: true,
  });

const editingMovieSuccess = state => mergeObject(state, {
    error: null,
    isLoading: false,
  });

const editingMovieFail = (state, action) => mergeObject(state, {
    error: action.error,
    isLoading: false,
  });

const receivingMovieStart = state => mergeObject(state, {
    error: null,
    isLoading: true,
    data: {},
  });

const receivingMovieSuccess = (state, action) => mergeObject(state, {
    error: null,
    isLoading: false,
    data: action.data.data.movie,
  });

const receivingMovieFail = (state, action) => mergeObject(state, {
    error: action.error.message,
    isLoading: false,
  });

const reducer = (state: {} = initialState, action: { type: string, error?: {}, data?: {} }) => {
  switch (action.type) {
    case actionTypes.ADDING_MOVIE_START:
      return addingMovieStart(state);
    case actionTypes.ADDING_MOVIE_SUCCESS:
      return addingMovieSuccess(state);
    case actionTypes.ADDING_MOVIE_FAIL:
      return addingMovieFail(state, action);
    case actionTypes.EDITING_MOVIE_START:
      return editingMovieStart(state);
    case actionTypes.EDITING_MOVIE_SUCCESS:
      return editingMovieSuccess(state);
    case actionTypes.EDITING_MOVIE_FAIL:
      return editingMovieFail(state, action);
    case actionTypes.RECEIVEING_MOVIE_START:
      return receivingMovieStart(state);
    case actionTypes.RECEIVEING_MOVIE_SUCCESS:
      return receivingMovieSuccess(state, action);
    case actionTypes.RECEIVEING_MOVIE_FAIL:
      return receivingMovieFail(state, action);
    default:
      return state;
  }
};

export default reducer;
