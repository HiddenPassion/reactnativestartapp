// @flow
import axios from '../../utility/axios';
import * as requestPath from './utility/RequestUrlPath';

const addMovie = data => axios.post(requestPath.MOVIE, data);
const editMovie = data => axios.patch(requestPath.MOVIE + data.id, data);
const receiveMovie = id => axios.get(requestPath.MOVIE + id);

export { addMovie, editMovie, receiveMovie };
