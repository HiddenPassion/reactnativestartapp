// @flow
import React from 'react';
import Input from './input';

/* eslint-disable no-return-assign, object-curly-newline, react/no-this-in-sfc */

type Props = {
  state: Object,
  onBlurHandler: Function,
  onSubmit: Function,
  onChange: Function,
};

const inputCreator = ({ state, onBlurHandler, onSubmit, onChange }: Props) => (
  <React.Fragment>
    <Input
      value={state.name.value}
      type="name"
      errorMessage={state.name.error}
      returnKeyType="next"
      onSubmitEditing={() => this.secondTextInput.focus()}
      blurOnSubmit={false}
      onBlur={() => onBlurHandler('name')}
      underlineColorAndroid={state.name.error ? 'red' : 'blue'}
      onChangeText={e => onChange('name', e)}
    />
    <Input
      value={state.actors.value}
      type="actors"
      errorMessage={state.actors.error}
      returnKeyType="next"
      setRef={input => (this.secondTextInput = input)}
      onSubmitEditing={() => this.thirdTextInput.focus()}
      blurOnSubmit={false}
      onBlur={() => onBlurHandler('actors')}
      underlineColorAndroid={state.actors.error ? 'red' : 'blue'}
      onChangeText={e => onChange('actors', e)}
    />
    <Input
      value={state.yearRelease.value}
      type="year release"
      errorMessage={state.yearRelease.error}
      returnKeyType="next"
      setRef={input => (this.thirdTextInput = input)}
      onSubmitEditing={() => this.fourthTextInput.focus()}
      blurOnSubmit={false}
      keyboardType="numeric"
      onBlur={() => onBlurHandler('yearRelease')}
      underlineColorAndroid={state.yearRelease.error ? 'red' : 'blue'}
      onChangeText={e => onChange('yearRelease', e)}
    />
    <Input
      value={state.length.value}
      type="length"
      errorMessage={state.length.error}
      returnKeyType="next"
      setRef={input => (this.fourthTextInput = input)}
      onSubmitEditing={() => this.fifthTextInput.focus()}
      blurOnSubmit={false}
      keyboardType="numeric"
      onBlur={() => onBlurHandler('length')}
      underlineColorAndroid={state.length.error ? 'red' : 'blue'}
      onChangeText={e => onChange('length', e)}
    />
    <Input
      value={state.poster.value}
      type="poster URL"
      errorMessage={state.poster.error}
      returnKeyType="next"
      setRef={input => (this.fifthTextInput = input)}
      onSubmitEditing={() => this.sixthTextInput.focus()}
      blurOnSubmit={false}
      onBlur={() => onBlurHandler('poster')}
      underlineColorAndroid={state.poster.error ? 'red' : 'blue'}
      onChangeText={e => onChange('poster', e)}
    />
    <Input
      value={state.description.value}
      type="description"
      multiline
      numberOfLines={4}
      errorMessage={state.description.error}
      returnKeyType="done"
      setRef={input => (this.sixthTextInput = input)}
      onSubmitEditing={onSubmit}
      onBlur={() => onBlurHandler('description')}
      underlineColorAndroid={state.length.error ? 'red' : 'blue'} // "transparent"
      onChangeText={e => onChange('description', e)}
    />
  </React.Fragment>
);

export default inputCreator;
