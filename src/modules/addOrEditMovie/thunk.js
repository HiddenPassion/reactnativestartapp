// @flow
import * as movieService from './movieService';
import * as actions from './movieActions';

const addMovie = (data: {}) => (dispatch: Function) => {
  dispatch(actions.addingMovieStart());

  return movieService
    .addMovie(data)
    .then(() => {
      dispatch(actions.addingMovieSuccess());
    })
    .catch((err) => {
      dispatch(actions.addingMovieFail(err));
    });
};

const editMovie = (data: {}) => (dispatch: Function) => {
  dispatch(actions.editingMovieStart());

  return movieService
    .editMovie(data)
    .then(() => {
      dispatch(actions.editingMovieSuccess());
    })
    .catch((err) => {
      dispatch(actions.editingMovieFail(err));
    });
};

const receiveMovie = (id: string) => (dispatch: Function) => {
  dispatch(actions.receiveingMovieStart());

  movieService
    .receiveMovie(id)
    .then((res) => {
      dispatch(actions.receiveingMovieSuccess(res));
    })
    .catch((err) => {
      dispatch(actions.receivingMovieFail(err));
    });
};

export { addMovie, editMovie, receiveMovie };
