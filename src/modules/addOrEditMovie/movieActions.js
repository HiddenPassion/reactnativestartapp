// @flow
import * as actionTypes from './actionTypes';

const addingMovieStart = () => ({
  type: actionTypes.ADDING_MOVIE_START,
});

const addingMovieFail = data => ({
  type: actionTypes.ADDING_MOVIE_FAIL,
  error: data,
});
const addingMovieSuccess = () => ({
  type: actionTypes.ADDING_MOVIE_SUCCESS,
});

const editingMovieStart = () => ({
  type: actionTypes.EDITING_MOVIE_START,
});

const editingMovieFail = data => ({
  type: actionTypes.EDITING_MOVIE_FAIL,
  error: data,
});

const editingMovieSuccess = () => ({
  type: actionTypes.EDITING_MOVIE_SUCCESS,
});

const receiveingMovieStart = () => ({
  type: actionTypes.RECEIVEING_MOVIE_START,
});
const receiveingMovieSuccess = (data: {}) => ({
  type: actionTypes.RECEIVEING_MOVIE_SUCCESS,
  data,
});

const receivingMovieFail = (err: {}) => ({
  type: actionTypes.RECEIVEING_MOVIE_FAIL,
  error: err,
});

export {
  addingMovieStart,
  addingMovieFail,
  addingMovieSuccess,
  editingMovieStart,
  editingMovieFail,
  editingMovieSuccess,
  receiveingMovieStart,
  receiveingMovieSuccess,
  receivingMovieFail,
};
