// @flow
export default (controlName: string, value: string) => {
  switch (controlName) {
    default: {
      if (!value) {
        return ' is required';
      }
      return '';
    }
  }
};
