// @flow
import axios from '../../utility/axios';
import * as constants from './utility/urlRequestConstants';

const addCinema = (data: Object) => axios.post(constants.CINEMA, data);

const getCinema = (data: { cinemaId: string }) => axios.get(`${constants.CINEMA}/?cinemaId=${data.cinemaId}`, { params: data });

const editCinema = (data: Object) => axios.patch(`${constants.CINEMA}/${data.id}`, data);

export default {
  addCinema,
  getCinema,
  editCinema,
};
