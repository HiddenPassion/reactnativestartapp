// @flow
import clone from 'clone';

import * as actionTypes from './actionTypes';
import initialState from './initialState';

const reducer = (state: Object = initialState, action: Object) => {
  switch (action.type) {
    case actionTypes.ADDING_CINEMA_START:
      return addingCinemaStart(state);
    case actionTypes.ADDING_CINEMA_FAIL:
      return addingCinemaSuccess(state);
    case actionTypes.ADDING_CINEMA_SUCCESS:
      return addingCinemaFail(state, action);
    case actionTypes.RECEIVING_CINEMA_START:
      return receivingCinemaStart(state);
    case actionTypes.RECEIVING_CINEMA_SUCCESS:
      return receivingCinemaSuccess(state, action);
    case actionTypes.RECEIVING_CINEMA_FAIL:
      return receivingCinemaFail(state, action);
    case actionTypes.EDITING_CINEMA_START:
      return editingCinemaStart(state);
    case actionTypes.EDITING_CINEMA_SUCCESS:
      return editingCinemaSuccess(state);
    case actionTypes.EDITING_CINEMA_FAIL:
      return editingCinemaFail(state, action);
    default:
      return state;
  }
};

const addingCinemaStart = state => ({
  ...clone(state),
  error: null,
  isLoading: true,
});

const addingCinemaSuccess = state => ({
  ...clone(state),
  error: null,
  isLoading: false,
});

const addingCinemaFail = (state, action) => ({
  ...clone(state),
  error: action.error,
  isLoading: false,
});

const receivingCinemaStart = state => ({
  ...clone(state),
  error: null,
  isLoading: true,
  data: {},
});

const receivingCinemaSuccess = (state, action) => ({
  ...clone(state),
  error: null,
  isLoading: false,
  data: action.data,
});

const receivingCinemaFail = (state, action) => ({
  ...clone(state),
  error: action.error,
  isLoading: false,
});

const editingCinemaStart = state => ({
  ...clone(state),
  error: null,
  isLoading: true,
});

const editingCinemaSuccess = state => ({
  ...clone(state),
  error: null,
  isLoading: false,
});

const editingCinemaFail = (state, action) => ({
  ...clone(state),
  error: action.error,
  isLoading: false,
});

export default reducer;
