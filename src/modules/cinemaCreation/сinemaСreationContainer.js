// @flow
import React from 'react';
import { connect } from 'react-redux';
import { compose, withStateHandlers, onlyUpdateForKeys } from 'recompose';

import UpsertCinemaComponent from './components/upsertCinemaComponent';
import asyncActions from './thunk';
import validate from './utility/validation';

type Controls = {
  value: string,
  error: ?string,
  touched: boolean,
};

type Props = {
  city: Controls,
  address: Controls,
  contactNumber: Controls,
  name: Controls,
  imageUrl: Controls,
  description: Controls,
};

const CinemaCreationContainer = (props: Props) => {
  const {
    name,
    description,
    imageUrl,
    city,
    address,
    contactNumber,
    onChange,
    onSubmit,
    onBlur,
  } = props;

  const isDisabled: boolean = !(
    name.value &&
    !name.error &&
    description.value &&
    !description.error &&
    imageUrl.value &&
    !imageUrl.error &&
    city.value &&
    !city.error &&
    address.value &&
    !address.error &&
    contactNumber.value &&
    !contactNumber.error
  );

  return (
    <UpsertCinemaComponent
      state={{
        name,
        description,
        imageUrl,
        city,
        address,
        contactNumber,
      }}
      onChange={onChange}
      onBlurHandler={onBlur}
      submitHandler={onSubmit}
      disabled={isDisabled}
      type="Add"
    />
  );
};

CinemaCreationContainer.navigatorStyle = {
  tabBarHidden: true,
};

const mapDispatchToProps = dispatch => ({
  addCinema(data, history) {
    dispatch(asyncActions.addCinema(data, history));
  },
});

const stateTemplate = {
  value: '',
  error: '',
  touched: false,
};

export default compose(
  onlyUpdateForKeys([
    'isLoading',
    'name',
    'description',
    'imageUrl',
    'city',
    'address',
    'contactNumber',
  ]),
  connect(
    null,
    mapDispatchToProps,
  ),
  withStateHandlers(
    {
      name: { ...stateTemplate },
      description: { ...stateTemplate },
      imageUrl: { ...stateTemplate },
      city: { ...stateTemplate },
      address: { ...stateTemplate },
      contactNumber: { ...stateTemplate },
    },
    {
      onChange: props => (controlName: string, value: string) => ({
        [controlName]: {
          ...props[controlName],
          value,
          error: props[controlName].touched
            ? validate(controlName, value)
            : props[controlName].error,
        },
      }),
      onSubmit: (props, realProps) => () => {
        const { name, description, imageUrl, address, contactNumber, city } = props;

        if (
          name.value &&
          !name.error &&
          description.value &&
          !description.error &&
          imageUrl.value &&
          !imageUrl.error &&
          address.value &&
          !address.error &&
          contactNumber.value &&
          !contactNumber.error &&
          city.value &&
          !city.error
        ) {
          const dataObj = {
            name: name.value,
            city: city.value,
            address: address.value,
            contactNumber: contactNumber.value,
            imageUrl: imageUrl.value,
            description: description.value,
          };

          realProps.addCinema(dataObj).then(() => {
            realProps.navigator.pop();
          });
        }
      },
      onBlur: props => controlName => ({
        [controlName]: {
          ...props[controlName],
          touched: true,
          error: validate(controlName, props[controlName].value),
        },
      }),
    },
  ),
)(CinemaCreationContainer);
