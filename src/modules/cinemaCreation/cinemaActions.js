// @flow
import * as actionTypes from './actionTypes';

const addingCinemaStart = () => ({
    type: actionTypes.ADDING_CINEMA_START,
  });

const addingCinemaFail = data => ({
    type: actionTypes.ADDING_CINEMA_FAIL,
    error: data,
  });

const addingCinemaSuccess = () => ({
    type: actionTypes.ADDING_CINEMA_SUCCESS,
  });

const receivingCinemaStart = () => ({
    type: actionTypes.RECEIVING_CINEMA_START,
  });

const receivingCinemaFail = err => ({
    type: actionTypes.RECEIVING_CINEMA_FAIL,
    error: err,
  });

const receivingCinemaSuccess = data => ({
    type: actionTypes.RECEIVING_CINEMA_SUCCESS,
    data,
  });

const editingCinemaStart = () => ({
    type: actionTypes.EDITING_CINEMA_START,
  });

const editingCinemaFail = () => ({
    type: actionTypes.EDITING_CINEMA_SUCCESS,
  });

const editingCinemaSuccess = err => ({
    type: actionTypes.EDITING_CINEMA_FAIL,
    error: err,
  });

export {
  addingCinemaStart,
  addingCinemaFail,
  addingCinemaSuccess,
  receivingCinemaStart,
  receivingCinemaFail,
  receivingCinemaSuccess,
  editingCinemaStart,
  editingCinemaFail,
  editingCinemaSuccess,
};
