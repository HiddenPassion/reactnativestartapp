// @flow
import cinemaService from './cinemaService';
import * as actions from './cinemaActions';

const addCinema = (data: Object) => (dispatch: Function) => {
  dispatch(actions.addingCinemaStart());

  return cinemaService
    .addCinema(data)
    .then(() => {
      dispatch(actions.addingCinemaSuccess());
    })
    .catch((err) => {
      dispatch(actions.addingCinemaFail(err));
    });
};

const receiveCinema = (data: Object) => (dispatch: Function) => {
  dispatch(actions.receivingCinemaStart());

  cinemaService
    .getCinema(data)
    .then((res) => {
      dispatch(actions.receivingCinemaSuccess(res));
    })
    .catch((err) => {
      dispatch(actions.receivingCinemaFail(err));
    });
};

const editCinema = (data: Object) => (dispatch: Function) => {
  dispatch(actions.editingCinemaStart());

  return cinemaService
    .editCinema(data)
    .then((res) => {
      dispatch(actions.editingCinemaSuccess(res));
    })
    .catch((err) => {
      dispatch(actions.editingCinemaFail(err));
    });
};

export default {
  addCinema,
  receiveCinema,
  editCinema,
};
