// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import isEqual from 'lodash/isEqual';

import CinemaCreation from './components/cinemaCreation';
import asyncActions from './thunk';
import { getCinemaById } from '../cinemaList/thunk';
import * as selector from './selector';

class CinemaEditingContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      city: '',
      isCityValid: false,
      address: '',
      isAddressValid: false,
      contactNumber: '',
      isContactNumberValid: false,
      name: '',
      isNameValid: false,
      imageUrl: '',
      isImageUrlValid: false,
      description: '',
      isDescriptionValid: false,
      error: '',
      isInitial: true,
    };
  }

  componentWillMount() {
    // if (queryString.parse(this.props.location.search).id) {
    //   const data = queryString.parse(this.props.location.search);

    //   this.props.getCinema(data.id);
    // } else {
    //   this.props.history.push(`/cinemas/${queryString.parse(this.props.location.search).id}`);
    // }
    const { getCinema, cinemaId } = this.props;

    getCinema(cinemaId);
  }

  componentWillReceiveProps(nextProps) {
    const { data, navigator } = this.props;
    if (this.state.isInitial && !nextProps.isLoading && !nextProps.error) {
      if (!isEqual(data, nextProps.data)) {
        this.setData(Array.isArray(data) ? nextProps.data : data);
      }
    } else if (nextProps.error) {
      navigator.pop();
    }
  }

  setData = (data) => {
    this.setState({
      city: data.city ? data.city : '',
      isCityValid: !!data.city,
      address: data.address ? data.address : '',
      isAddressValid: !!data.address,
      contactNumber: data.contactNumber ? data.contactNumber : '',
      isContactNumberValid: !!data.contactNumber,
      name: data.name ? data.name : '',
      isNameValid: !!data.name,
      imageUrl: data.imageUrl ? data.imageUrl : '',
      isImageUrlValid: !!data.imageUrl,
      description: data.description ? data.description : '',
      isDescriptionValid: !!data.description,
      isInitial: false,
    });
  };

  onCityChange = (e) => {
    const value = e.target.value;

    if (!value) {
      this.setState({
        city: value,
        isCityValid: false,
        error: 'Field city is required',
      });
    } else if (value.length <= 3) {
      this.setState({
        city: value,
        isCityValid: false,
        error: 'Field city should be more than 3 letters',
      });
    } else {
      this.setState({
        city: value,
        isCityValid: true,
        error: '',
      });
    }
  };

  onAddressChange = (e) => {
    const value = e.target.value;

    if (!value) {
      this.setState({
        address: value,
        isAddressValid: false,
        error: 'Field address is required',
      });
    } else if (!/([A-Z]{1,1}[a-z]+,)(\s[A-Z]{1,1}[a-z]+\sstreet,)(\s\d+)/g.test(value)) {
      this.setState({
        address: value,
        isAddressValid: false,
        error: "Field address doesn't match the pattern (Minsk, Golodeda street, 37)",
      });
    } else {
      this.setState({
        address: value,
        isAddressValid: true,
        error: '',
      });
    }
  };

  onMobileNumberChange = (e) => {
    const value = e.target.value;

    if (!value) {
      this.setState({
        contactNumber: value,
        isContactNumberValid: false,
        error: 'Field mobile number is required',
      });
    } else if (value.length < 17 || !/\+375\([0-9]{2}\)[0-9]{3}-[0-9]{2}-[0-9]{2}/gi.test(value)) {
      this.setState({
        contactNumber: value,
        isContactNumberValid: false,
        error: "Field mobile number doesn't match the pattern (+375(xx)xxx-xx-xx)",
      });
    } else {
      this.setState({
        contactNumber: value,
        isContactNumberValid: true,
        error: '',
      });
    }
  };

  onCinemaNameChange = (e) => {
    const value = e.target.value;

    if (!value) {
      this.setState({
        name: value,
        isNameValid: false,
        error: 'Field cinema name is required',
      });
    } else if (value.length <= 3) {
      this.setState({
        name: value,
        isNameValid: false,
        error: 'Field cinema name should be more than 3 letters',
      });
    } else {
      this.setState({
        name: value,
        isNameValid: true,
        error: '',
      });
    }
  };

  onImageUrlChange = (e) => {
    const value = e.target.value;

    if (!value) {
      this.setState({
        imageUrl: value,
        isImageUrlValid: false,
        error: 'Field image url is required',
      });
    } else if (value.length < 7 || !/http(|s):\/\/(\w|.){3}/g.test(value)) {
      this.setState({
        imageUrl: value,
        isImageUrlValid: false,
        error: "Field image url doesn't match the pattern (https://somepath)",
      });
    } else {
      this.setState({
        imageUrl: value,
        isImageUrlValid: true,
        error: '',
      });
    }
  };

  onDescriptionChange = (e) => {
    const value = e.target.value;

    if (!value) {
      this.setState({
        description: value,
        isDescriptionValid: false,
        error: 'Field description is required',
      });
    } else {
      this.setState({
        description: value,
        isDescriptionValid: true,
        error: '',
      });
    }
  };

  onSubmit = (e) => {
    e.preventDefault();

    this.props
      .onEditCinema({
        id: this.props.data.id,
        name: this.state.name,
        city: this.state.city,
        address: this.state.address,
        contactNumber: this.state.contactNumber,
        imageUrl: this.state.imageUrl,
        description: this.state.description,
      })
      .then(() => {
        this.props.history.push(`/cinema/${queryString.parse(this.props.location.search).id}`);
      });
  };

  hasError = () => {
    if (this.state.error) {
      return this.state.error;
    }
    return '';
  };

  render() {
    const disabled = !(
      this.state.isCityValid
      && this.state.isAddressValid
      && this.state.isContactNumberValid
      && this.state.isNameValid
      && this.state.isImageUrlValid
      && this.state.isDescriptionValid
    );

    return (
      <div>
        <CinemaCreation
          state={this.state}
          onCityChange={this.onCityChange}
          onAddressChange={this.onAddressChange}
          onMobileNumberChange={this.onMobileNumberChange}
          onCinemaNameChange={this.onCinemaNameChange}
          onImageUrlChange={this.onImageUrlChange}
          onDescriptionChange={this.onDescriptionChange}
          onSubmit={this.onSubmit}
          hasError={this.hasError}
          btnName="EDIT"
          disabled={disabled}
        />
      </div>
    );
  }
}

CinemaEditingContainer.propTypes = {
  error: PropTypes.any,
  isLoading: PropTypes.bool,
  getCinema: PropTypes.func.isRequired,
  onEditCinema: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  error: selector.getErrorMessage(state),
  isLoading: selector.getLoadingStatus(state),
  data: selector.getData(state),
});

const mapDispatchToProps = dispatch => ({
  getCinema: data => dispatch(getCinemaById(data)),
  onEditCinema: data => dispatch(asyncActions.editCinema(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CinemaEditingContainer);
