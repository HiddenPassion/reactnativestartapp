// @flow
import React from 'react';
import Input from './input';

/* eslint-disable no-return-assign, object-curly-newline, react/no-this-in-sfc */

type Props = {
  state: Object,
  onBlurHandler: Function,
  onSubmit: Function,
  onChange: Function,
};

const inputCreator = ({ state, onBlurHandler, onSubmit, onChange }: Props) => (
  <React.Fragment>
    <Input
      value={state.name.value}
      type="name"
      errorMessage={state.name.error}
      returnKeyType="next"
      onSubmitEditing={() => this.secondTextInput.focus()}
      blurOnSubmit={false}
      onBlur={() => onBlurHandler('name')}
      underlineColorAndroid={state.name.error ? 'red' : 'blue'}
      onChangeText={e => onChange('name', e)}
    />
    <Input
      value={state.city.value}
      type="city"
      errorMessage={state.city.error}
      returnKeyType="next"
      setRef={input => (this.secondTextInput = input)}
      onSubmitEditing={() => this.thirdTextInput.focus()}
      blurOnSubmit={false}
      onBlur={() => onBlurHandler('city')}
      underlineColorAndroid={state.city.error ? 'red' : 'blue'}
      onChangeText={e => onChange('city', e)}
    />
    <Input
      value={state.address.value}
      type="address"
      errorMessage={state.address.error}
      returnKeyType="next"
      setRef={input => (this.thirdTextInput = input)}
      onSubmitEditing={() => this.fourthTextInput.focus()}
      blurOnSubmit={false}
      keyboardType="numeric"
      onBlur={() => onBlurHandler('address')}
      underlineColorAndroid={state.address.error ? 'red' : 'blue'}
      onChangeText={e => onChange('address', e)}
    />
    <Input
      value={state.imageUrl.value}
      type="imageUrl"
      errorMessage={state.imageUrl.error}
      returnKeyType="next"
      setRef={input => (this.fourthTextInput = input)}
      onSubmitEditing={() => this.fifthTextInput.focus()}
      blurOnSubmit={false}
      keyboardType="numeric"
      onBlur={() => onBlurHandler('imageUrl')}
      underlineColorAndroid={state.imageUrl.error ? 'red' : 'blue'}
      onChangeText={e => onChange('imageUrl', e)}
    />
    <Input
      value={state.contactNumber.value}
      type="contact number"
      errorMessage={state.contactNumber.error}
      returnKeyType="next"
      setRef={input => (this.fifthTextInput = input)}
      onSubmitEditing={() => this.sixthTextInput.focus()}
      blurOnSubmit={false}
      onBlur={() => onBlurHandler('contactNumber')}
      underlineColorAndroid={state.contactNumber.error ? 'red' : 'blue'}
      onChangeText={e => onChange('contactNumber', e)}
    />
    <Input
      value={state.description.value}
      type="description"
      multiline
      numberOfLines={4}
      errorMessage={state.description.error}
      returnKeyType="done"
      setRef={input => (this.sixthTextInput = input)}
      onSubmitEditing={onSubmit}
      onBlur={() => onBlurHandler('description')}
      underlineColorAndroid={state.description.error ? 'red' : 'blue'} // "transparent"
      onChangeText={e => onChange('description', e)}
    />
  </React.Fragment>
  );

export default inputCreator;
