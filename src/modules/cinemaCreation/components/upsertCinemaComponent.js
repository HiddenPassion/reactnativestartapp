// @flow
import React from 'react';
import {
  View,
  StyleSheet,
  ActivityIndicator,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import InputCreator from './inputCreator';
import Button from './button';

type Props = {
  isLoading?: boolean,
  type: string,
  submitHandler: Function,
  disabled: boolean,
  state: {},
  onBlurHandler: Function,
  onChange: Function,
};

const upserCinemaComponent = ({
  type,
  isLoading,
  submitHandler,
  disabled,
  state,
  onBlurHandler,
  onChange,
}: Props) => (
  <View style={styles.container}>
    <ScrollView style={styles.scrollView}>
      <KeyboardAvoidingView behavior="padding">
        <View style={styles.inputContainer}>
          <InputCreator
            state={state}
            onBlurHandler={onBlurHandler}
            onChange={onChange}
            onSubmit={submitHandler}
          />
        </View>
        <View style={styles.buttonContainer}>
          {isLoading ? (
            <ActivityIndicator size="small" color="blue" />
          ) : (
            <Button onSubmit={submitHandler} type={type} disabled={disabled} />
          )}
        </View>
      </KeyboardAvoidingView>
    </ScrollView>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    backgroundColor: '#eee',
  },
  scrollView: {
    flex: 1,
    width: '100%',
  },
  inputContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default upserCinemaComponent;
