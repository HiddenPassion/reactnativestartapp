// @flow
import React from 'react';
import PropTypes from 'prop-types';
import {
 View, Text, TextInput, StyleSheet,
} from 'react-native';

type Props = {
  errorMessage: string,
  setRef: Function,
  type: string,
  props: Object,
};

// eslint-disable-next-line
const Input = ({ errorMessage, setRef, type, ...props }: Props) => (
  <View style={styles.container}>
    <View style={[styles.inputFieldContainer]}>
      <TextInput
        {...props}
        ref={setRef}
        placeholder={type && type[0].toUpperCase() + type.slice(1)}
        style={styles.inputField}
      />
    </View>
    <Text style={[errorMessage ? styles.invalidLabel : styles.hidden]}>{type + errorMessage}</Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    width: '80%',
    position: 'relative',
  },
  invalidLabel: {
    fontSize: 16,
    paddingLeft: 3,
    position: 'absolute',
    top: 48, // depends on height of inputContainer
    left: 5,
    color: 'red',
  },
  inputFieldContainer: {
    marginTop: 10,
  },
  inputField: {
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderTopWidth: 1,
    borderColor: 'blue',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: 'rgb(230, 250, 252)',
    padding: 7,
    paddingTop: 15,
    fontSize: 20,
    marginBottom: 10,
  },
  hidden: {
    display: 'none',
  },
});

Input.propTypes = {
  type: PropTypes.string.isRequired,
  value: PropTypes.string,
};

export default Input;
