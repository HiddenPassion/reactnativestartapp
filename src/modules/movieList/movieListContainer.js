// @flow
import React from 'react';
import PropTypes from 'prop-types';
import isEqual from 'lodash/isEqual';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import {
 lifecycle, compose, onlyUpdateForKeys, setPropTypes, withHandlers,
} from 'recompose';

import * as asyncActions from './thunk';
import * as selector from './selector';

import MovieList from './components/movieList';

// eslint-disable-next-line
const MovieListContainer = ({ movies, isLoading, getMovies, onMoviePosterClick }) => (
  <MovieList
    movieList={movies}
    isLoading={isLoading}
    onMoviePosterClick={onMoviePosterClick}
    onRefresh={getMovies}
  />
);

const mapDispatchToProps = dispatch => ({
  getMovies: params => dispatch(asyncActions.getMoviesWithFilter(params)),
  // getCinemaById: (id) => dispatch(asyncActionsCinemas.getCinemaById(id)),
  // changeMovieStatus: params => dispatch(asyncActions.changeMovieStatus(params)),
  // removeFromAssigned: params => dispatch(asyncActions.removeFromAssigned(params)),
  // deleteMovie: id => dispatch(asyncActions.deleteMovie(id)),
});

const mapStateToProps = state => ({
  movies: selector.getMovies(state),
  isLoading: selector.getLoadingStatus(state),
});

// export default connect(mapStateToProps, mapDispatchToProps)(MovieListContainer);

export default compose(
  onlyUpdateForKeys(['movie', 'isLoading']),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  setPropTypes({
    isLoading: PropTypes.bool,
    movies: PropTypes.array.isRequired,
  }),
  withHandlers({
    onNavigatorEvent: props => (event) => {
      if (event.type === 'ScreenChangedEvent' && event.id === 'willAppear') {
        if (isEmpty(props.movies)) {
          props.getMovies();
        }
      }

      if (event.type === 'NavBarButtonPress' && event.id === 'sideDrawerToggle') {
        props.navigator.toggleDrawer({
          side: 'left',
        });
      }

      if (event.type === 'NavBarButtonPress' && event.id === 'addButton') {
        props.navigator.push({
          screen: 'navigation.addMoviePage',
          title: 'Adding Movie',
        });
      }
    },
    onMoviePosterClick: props => (id) => {
      const selectedMovie = props.movies.find(movie => movie.id === id);

      props.navigator.push({
        screen: 'navigation.moviePage',
        title: selectedMovie.name,
        passProps: {
          movie: selectedMovie,
        },
      });
    },
  }),
  lifecycle({
    componentWillMount() {
      this.props.navigator.setOnNavigatorEvent(this.props.onNavigatorEvent);
    },
    shouldComponentUpdate(nextProps) {
      const { isLoading, movies } = this.props;
      return isLoading !== nextProps.isLoading || !isEqual(movies, nextProps.movies);
    },
  }),
)(MovieListContainer);
