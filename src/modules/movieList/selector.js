// @flow
import { createSelector } from 'reselect';

const movieState = state => state.movies;

const getLoadingStatus = createSelector(movieState, state => state.isMoviesLoading);
const getErrorMessage = createSelector(movieState, state => state.error);
const getMovies = createSelector(movieState, state => state.moviesListLocaly);
const getOptionsLoadingStatus = createSelector(movieState, state => state.isOptionsLoading);
const getOptionsErrorMessage = createSelector(movieState, state => state.error);
const getNameList = createSelector(movieState, state => state.nameList);
const getYearReleaseList = createSelector(movieState, state => state.yearReleaseList);

export {
  getMovies,
  getLoadingStatus,
  getErrorMessage,
  getOptionsLoadingStatus,
  getOptionsErrorMessage,
  getNameList,
  getYearReleaseList,
};
