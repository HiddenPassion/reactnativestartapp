// @flow
import * as actionTypes from '../actions/actionTypes';
import mergeObject from '../../../utility/mergeObjects';
import initialState from './initialState';

const gettingOptionsStart = state => mergeObject(state, {
    optionsError: null,
    isOptionsLoading: true,
    nameList: [],
    yearReleaseList: [],
  });

const gettingOptionsSuccess = (state, action) => mergeObject(state, {
    optionsError: null,
    isOptionsLoading: false,
    nameList: action.data.name,
    yearReleaseList: action.data.yearRelease,
  });

const gettingOptionsFail = (state, action) => mergeObject(state, {
    optionsError: action.error,
    nameList: [],
    yearReleaseList: [],
    isOptionsLoading: false,
  });

const gettingMoviesWithFilterStart = state => mergeObject(state, {
    moviesError: null,
    isMoviesLoading: true,
    moviesList: [],
    moviesListLocaly: [],
  });

const gettingMoviesWithFilterSuccess = (state, action) => mergeObject(state, {
    moviesError: null,
    isMoviesLoading: false,
    moviesList: action.data.data.movies,
    moviesListLocaly: action.data.data.movies,
  });

const gettingMoviesWithFilterFail = (state, action) => mergeObject(state, {
    moviesError: action.error,
    isMoviesLoading: false,
    moviesList: [],
    moviesListLocaly: [],
  });

const gettingMoviesWithFilterLocaly = (state, action) => mergeObject(state, {
    moviesListLocaly: action.data,
  });

const fetchFreeMoviesStart = state => mergeObject(state, {
    moviesError: null,
    isMoviesLoading: true,
    freeMovies: [],
  });

const fetchFreeMoviesSuccess = (state, action) => mergeObject(state, {
    moviesError: null,
    isMoviesLoading: false,
    freeMovies: action.freeMovies,
  });

const fetchFreeMoviesFail = (state, action) => mergeObject(state, {
    moviesError: action.error,
    isMoviesLoading: false,
  });

const changeMovieStatusStart = state => mergeObject(state, {
    error: null,
    isMoviesLoading: true,
  });

const changeMovieStatusFail = (state, action) => mergeObject(state, {
    error: action.error,
    isMoviesLoading: false,
  });

const changeMovieStatusSuccess = state => mergeObject(state, {
    error: null,
    isMoviesLoading: false,
  });

const reducer = (
  state: {} = initialState,
  action: { type: string, data?: any, error?: any, freeMovies: Array<{}> },
) => {
  switch (action.type) {
    case actionTypes.GETTING_MOVIE_OPTIONS_START:
      return gettingOptionsStart(state);
    case actionTypes.GETTING_MOVIE_OPTIONS_SUCCESS:
      return gettingOptionsSuccess(state, action);
    case actionTypes.GETTING_MOVIE_OPTIONS_FAIL:
      return gettingOptionsFail(state, action);

    case actionTypes.GETTING_MOVIES_WITH_FILTER_START:
      return gettingMoviesWithFilterStart(state);
    case actionTypes.GETTING_MOVIES_WITH_FILTER_SUCCESS:
      return gettingMoviesWithFilterSuccess(state, action);
    case actionTypes.GETTING_MOVIES_WITH_FILTER_FAIL:
      return gettingMoviesWithFilterFail(state, action);
    case actionTypes.GETTING_MOVIES_WITH_FILTER_LOCALY:
      return gettingMoviesWithFilterLocaly(state, action);

    case actionTypes.FETCH_FREE_MOVIES_START:
      return fetchFreeMoviesStart(state);
    case actionTypes.FETCH_FREE_MOVIES_FAIL:
      return fetchFreeMoviesFail(state, action);
    case actionTypes.FETCH_FREE_MOVIES_SUCCESS:
      return fetchFreeMoviesSuccess(state, action);

    case actionTypes.CHANGE_MOVIE_STATUS_START:
      return changeMovieStatusStart(state);
    case actionTypes.CHANGE_MOVIE_STATUS_FAIL:
      return changeMovieStatusFail(state, action);
    case actionTypes.CHANGE_MOVIE_STATUS_SUCCESS:
      return changeMovieStatusSuccess(state);
    default:
      return state;
  }
};

export default reducer;
