// @flow
export default {
  isOptionsLoading: false,
  optionsError: null,
  nameList: [],
  yearReleaseList: [],
  moviesList: [],
  moviesListLocaly: [],
  freeMovies: [],
  isMoviesLoading: false,
  moviesError: null,
};
