// @flow
import * as actionTypes from './actionTypes';

const gettingOptionsStart = () => ({
  type: actionTypes.GETTING_MOVIE_OPTIONS_START,
});

const gettingOptionsFail = data => ({
  type: actionTypes.GETTING_MOVIE_OPTIONS_FAIL,
  error: data,
});

const gettingOptionsSuccess = data => ({
  type: actionTypes.GETTING_MOVIE_OPTIONS_SUCCESS,
  data: data.data.options,
});

const gettingMoviesWithFilterStart = () => ({
  type: actionTypes.GETTING_MOVIES_WITH_FILTER_START,
});

const gettingMoviesWithFilterSuccess = data => ({
  type: actionTypes.GETTING_MOVIES_WITH_FILTER_SUCCESS,
  data,
});

const gettingMoviesWithFilterFail = err => ({
  type: actionTypes.GETTING_MOVIES_WITH_FILTER_FAIL,
  error: err,
});

const gettingMoviesWithFilterLocaly = data => ({
  type: actionTypes.GETTING_MOVIES_WITH_FILTER_LOCALY,
  data,
});

const fetchFreeMoviesStart = () => ({
  type: actionTypes.FETCH_FREE_MOVIES_START,
});

const fetchFreeMoviesFail = data => ({
  type: actionTypes.FETCH_FREE_MOVIES_FAIL,
  error: data,
});

const fetchFreeMoviesSuccess = data => ({
  type: actionTypes.FETCH_FREE_MOVIES_SUCCESS,
  freeMovies: data.data.movies,
});

const changeMovieStatusStart = () => ({
  type: actionTypes.CHANGE_MOVIE_STATUS_START,
});

const changeMovieStatusFail = data => ({
  type: actionTypes.CHANGE_MOVIE_STATUS_FAIL,
  error: data,
});

const changeMovieStatusSuccess = () => ({
  type: actionTypes.CHANGE_MOVIE_STATUS_SUCCESS,
});

export {
  gettingOptionsStart,
  gettingOptionsSuccess,
  gettingOptionsFail,
  gettingMoviesWithFilterStart,
  gettingMoviesWithFilterSuccess,
  gettingMoviesWithFilterFail,
  gettingMoviesWithFilterLocaly,
  fetchFreeMoviesStart,
  fetchFreeMoviesFail,
  fetchFreeMoviesSuccess,
  changeMovieStatusStart,
  changeMovieStatusFail,
  changeMovieStatusSuccess,
};
