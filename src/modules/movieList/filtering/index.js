// @flow
import Filtering from './filteringContainer/filteringContainer';
import filteringReducer from '../reducers/movieListReducer';

export { Filtering, filteringReducer };
