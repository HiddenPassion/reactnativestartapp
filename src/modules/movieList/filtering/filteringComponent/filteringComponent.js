// @flow
import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

type Props = {
  onSubmit: Function,
  inputFieldValue: string,
  onInputChange: Function,
};

const filteringComponent = ({ onSubmit, inputFieldValue, onInputChange }: Props) => (
  <View style={styles.container}>
    <View style={styles.inputContainer}>
      <Icon style={styles.icon} name="md-search" size={24} />
      <TextInput
        onSubmitEditing={onSubmit}
        returnKeyType="search"
        style={styles.textInput}
        onChangeText={onInputChange}
        underlineColorAndroid="transparent"
        value={inputFieldValue}
        placeholder="Input film name"
      />
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    position: 'relative',
    overflow: 'visible',
    paddingTop: 8,
    paddingBottom: 30,
    backgroundColor: '#eee',
  },
  icon: {
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  inputContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderWidth: 2,
    borderRadius: 20,
    marginHorizontal: 3,
  },
  textInput: {
    flex: 1,
    paddingVertical: 5,
    paddingRight: 10,
    paddingLeft: 0,
    marginRight: 20,
    backgroundColor: '#fff',
    color: '#424242',
    fontSize: 18,
  },
});

export default filteringComponent;
