// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as selector from '../../selector';
import * as actions from '../../thunk';
import FilteringComponent from '../filteringComponent/filteringComponent';

type Props = {
  getOptions: Function,
  nameList: Array<{}>,
  getMoviesLocaly: Function,
};

class Filtering extends Component<Props> {
  state = {
    name: '',
  };

  componentWillMount() {
    // eslint-disable-next-line
    const { getOptions } = this.props;
    getOptions();
  }

  setParams = (data: { name: string }) => ({
    name: data.name ? data.name : '',
  });

  onInputChange = (searchableName) => {
    this.setState(
      {
        name: searchableName,
      },
      () => {
        this.onSubmit();
      },
    );
  };

  onSubmit = () => {
    const { getMoviesLocaly } = this.props;
    getMoviesLocaly(this.setParams(this.state));
  };

  render() {
    const { name } = this.state;
    const { nameList } = this.props;
    return (
      <FilteringComponent
        onSubmit={this.onSubmit}
        inputFieldValue={name}
        onInputChange={this.onInputChange}
        nameList={nameList}
      />
    );
  }
}

const mapStateToProps = state => ({
  isOptionsLoading: selector.getOptionsLoadingStatus(state),
  optionsError: selector.getOptionsErrorMessage(state),
  nameList: selector.getNameList(state),
  yearReleaseList: selector.getYearReleaseList(state),
});

const mapDispatchToProps = dispatch => ({
  getOptions: () => dispatch(actions.getOptions()),
  getMovies: data => dispatch(actions.getMoviesWithFilter(data)),
  getMoviesLocaly: data => dispatch(actions.getMoviesWithFilterLocaly(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Filtering);
