// @flow
import * as filteringService from './movieListService';
import * as actions from './actions/movieListActions';

const getOptions = () => (dispatch: Function) => {
  dispatch(actions.gettingOptionsStart());
  filteringService
    .getOptions()
    .then((res) => {
      dispatch(actions.gettingOptionsSuccess(res));
    })
    .catch((err) => {
      dispatch(actions.gettingOptionsFail(err));
    });
};

const getMoviesWithFilter = (filter?: {}) => (dispatch: Function) => {
  dispatch(actions.gettingMoviesWithFilterStart());

  filteringService
    .getMoviesWithFilter(filter)
    .then((res) => {
      dispatch(actions.gettingMoviesWithFilterSuccess(res));
    })
    .catch((err) => {
      dispatch(actions.gettingMoviesWithFilterFail(err));
    });
};

const fetchFreeMovies = (filter: {}) => (dispatch: Function) => {
  dispatch(actions.fetchFreeMoviesStart());

  filteringService
    .getMoviesWithFilter(filter)
    .then((res) => {
      dispatch(actions.fetchFreeMoviesSuccess(res));
    })
    .catch((err) => {
      dispatch(actions.fetchFreeMoviesFail(err));
    });
};

const changeMovieStatus = (data: { cinemaId: string }) => (dispatch: Function) => {
  dispatch(actions.changeMovieStatusStart());

  filteringService
    .changeMovieStatus(data)
    .then(() => {
      dispatch(actions.changeMovieStatusSuccess());
      dispatch(getMoviesWithFilter({ cinemaId: data.cinemaId, isArchived: false }));
    })
    .catch(() => {
      dispatch(actions.changeMovieStatusFail());
    });
};

const removeFromAssigned = (data: { relId: string, cinemaId: string }) => (dispatch: Function) => {
  dispatch(actions.changeMovieStatusStart());

  filteringService
    .removeFromAssigned(data.relId)
    .then(() => {
      dispatch(actions.changeMovieStatusSuccess());
      dispatch(getMoviesWithFilter({ cinemaId: data.cinemaId }));
    })
    .catch(() => {
      dispatch(actions.changeMovieStatusFail());
    });
};

const deleteMovie = (movieId: string) => (dispatch: Function) => {
  dispatch(actions.changeMovieStatusStart());

  filteringService
    .deleteMovie(movieId)
    .then(() => {
      dispatch(actions.changeMovieStatusSuccess());
      dispatch(getMoviesWithFilter());
    })
    .catch(() => {
      dispatch(actions.changeMovieStatusFail());
    });
};

const getMoviesWithFilterLocaly = (filter: { name: string }) => (
  dispatch: Function,
  getState: Function,
) => {
  const res = getState().movies.moviesList.filter(
    ({ name }) => name.toLowerCase().indexOf(filter.name.toLowerCase()) > -1,
  );
  dispatch(actions.gettingMoviesWithFilterLocaly(res));
};

export {
  getMoviesWithFilterLocaly,
  getOptions,
  getMoviesWithFilter,
  fetchFreeMovies,
  changeMovieStatus,
  removeFromAssigned,
  deleteMovie,
};
