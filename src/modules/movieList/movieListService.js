// @flow
import axios from '../../utility/axios';
import * as requestPath from './utility/requesUrlConstants';

const getOptions = () => axios.get(requestPath.GET_OPTIONS);

const getMoviesWithFilter = filter => axios.get(requestPath.GET_DATA_WITH_FILTER, { params: filter });

const changeMovieStatus = data => axios.patch(requestPath.ASSIGNED_MOVIES + data.cinemaId, {
    movieId: data.movieId,
    isArchived: data.isArchived,
  });

const removeFromAssigned = data => axios.delete(requestPath.ASSIGNED_MOVIES + data);

const deleteMovie = id => axios.delete(requestPath.GET_DATA_WITH_FILTER + id);

export {
 getOptions, getMoviesWithFilter, changeMovieStatus, removeFromAssigned, deleteMovie,
};
