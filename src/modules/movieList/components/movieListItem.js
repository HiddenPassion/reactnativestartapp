// @flow
import React from 'react';
import {
 View, Image, StyleSheet, TouchableWithoutFeedback,
} from 'react-native';
import PropTypes from 'prop-types';

type Props = {
  onMoviePosterClick: Function,
  movie: {
    poster: string,
  },
};

const movieListItem = ({ onMoviePosterClick, movie }: Props) => (
  <TouchableWithoutFeedback onPress={onMoviePosterClick}>
    <View style={styles.container}>
      <Image source={{ uri: movie.poster }} style={styles.image} resizeMode="stretch" />
    </View>
  </TouchableWithoutFeedback>
);

movieListItem.propTypes = {
  movie: PropTypes.object.isRequired,
  onMoviePosterClick: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: '90%',
    height: 350,
    borderColor: '#ccc',
    borderWidth: 0.7,
  },
});

export default movieListItem;
