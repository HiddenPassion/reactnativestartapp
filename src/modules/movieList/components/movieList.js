// @flow
import React from 'react';
import PropTypes from 'prop-types';
import { View, FlatList, StyleSheet } from 'react-native';
import MovieListItem from './movieListItem';
import Filtering from '../filtering/filteringContainer/filteringContainer';

const separator = () => (
  <View style={styles.separator}>
    <View style={styles.separatorLine} />
  </View>
);

type Props = {
  isLoading: ?boolean,
  onRefresh: Function,
  movieList: {},
  onMoviePosterClick: Function,
};

const MovieList = ({
 isLoading, onRefresh, movieList, onMoviePosterClick,
}: Props) => (
  <View style={styles.container}>
    <FlatList
      ListHeaderComponent={Filtering}
      refreshing={isLoading}
      onRefresh={onRefresh}
      data={movieList}
      keyExtractor={item => item.id}
      renderItem={({ item }) => (
        <MovieListItem movie={item} onMoviePosterClick={() => onMoviePosterClick(item.id)} />
      )}
      style={styles.flatList}
      ItemSeparatorComponent={separator}
    />
  </View>
);

MovieList.propTypes = {
  onMoviePosterClick: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  movieList: PropTypes.array.isRequired,
  onRefresh: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eee',
    alignItems: 'center',
  },
  flatList: {
    width: '100%',
  },
  separator: {
    width: '100%',
    backgroundColor: '#eee',
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  separatorLine: {
    height: 1.5,
    width: '94%',
    backgroundColor: '#ccc',
  },
});

export default MovieList;
