import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import queryString from 'query-string';

import buttonsParams from '../../../utility/contentParams';
import * as constants from '../../roomList/utility/urlConstants';
import classes from '../../common/sidebar/sidebar.scss';

import SidebarButton from '../../common/sidebar/sidebarButtons';
import Modal from '../../common/modalDelete/modalDelete';
import { deleteCinema } from '../../cinemaList/thunk';


class CinemaPageSidebar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isDeleteModalOpen: false,
        };
    }

    componentWillMount() {
        if (queryString.parse(this.props.location.search).id) {
            this.cinemaId = queryString.parse(this.props.location.search).id;
        } else {
            this.props.history.push(constants.CINEMAS_PAGE);
        }
    }

    onClickDelete = () => {
        this.props.onDeleteCinema(this.cinemaId);
        this.props.history.push(`/cinemas`);
    };

    onModalCancelClick = () => {
        this.setState( () =>
            ({isDeleteModalOpen: false})
        );
    };

    onModalOpenClick = () => {
        this.setState( () =>
            ({isDeleteModalOpen: true})
        );
    };

    render() {
        return (
            <div>
                <Modal
                    onModalDeleteClick={this.onClickDelete}
                    onModalCancelClick={this.onModalCancelClick}
                    id={this.cinemaId}
                    isOpen={this.state.isDeleteModalOpen}/>
                <SidebarButton
                    cinemaId={this.cinemaId}
                    buttonParams={buttonsParams.cinemaPage.buttons}>
                    <button onClick={this.onModalOpenClick} className={classes[`aside_button`]}>
                        <FontAwesomeIcon
                            icon={faTrashAlt}
                            className={classes[`aside_button-link-svg`]}/>Delete Cinema
                    </button>
                </SidebarButton>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    onDeleteCinema: (id) => dispatch(deleteCinema(id)),
});

export default connect(null, mapDispatchToProps)(CinemaPageSidebar);