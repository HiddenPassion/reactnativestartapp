import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import { faPhone } from '@fortawesome/free-solid-svg-icons';
import { faPlusSquare } from '@fortawesome/free-solid-svg-icons';

import MovieList from '../../movieList/components/movieList';
import AssignMovie from './assignMovie';
import { AdditionList } from '../../addition';

import classes from '../scss/cinemaPage.scss';

function CinemaPage(props) {
    return (
        <div className={classes.cinema} onClick={props.onClickOverlay}>
            <h2 className={classes['cinema__h2']}>{props.cinema.name}</h2>
            <div className={classes['cinema__content']}>

                <div className={classes['cinema__content-image']}>
                    <img className={classes['cinema__content-image-img']} src={props.cinema.imageUrl}/>
                </div>

                <article className={classes['cinema__content-info']}>
                    <address className={classes['cinema__address']}>
                        <FontAwesomeIcon
                            className={classes[`cinema__address-svg`]}
                            icon={faMapMarkerAlt}/> {props.cinema.address}, {props.cinema.city}
                    </address>
                    <div className={classes[`cinema__contacts`]}>
                        <FontAwesomeIcon
                            icon={faPhone}/> {props.cinema.contactNumber}
                    </div>
                </article>

            </div>

            <div className={classes['cinema__description']}>
                <h3>About:</h3>
                <p className={classes['cinema__content-description']}>
                    {props.cinema.description}
                </p>
            </div>
            <hr/>
            <hr/>
            <div className={classes['cinema__isOnToday']}>
                <h3>Additions:</h3>
                <AdditionList
                    location={props.location}
                    history={props.history}/>
            </div>
            <hr/>
            <div className={classes['cinema__isOnToday']}>
                <div className={classes['cinema__isOnToday-header']}>
                    <h3>On screen today:</h3>
                    <span onClick={props.onClickAddNew}
                          className={classes['cinema__addNew']}>
                        <FontAwesomeIcon
                        className={classes[`cinema__addNew-svg`]}
                        icon={faPlusSquare}/>
                        Add New
                    </span>
                </div>
                {props.isAddingNew
                    ? <AssignMovie
                        onCloseWindow={props.onCloseWindow}
                        onAssigningMovie={props.onAssigningMovie}
                        onChangeInputTitle={props.onChangeInputTitle}
                        cinemaId={props.cinema.id}
                        movies={props.freeMovies}/>
                    : null}
                <MovieList
                    isAssignedMovies={!!props.cinema.id}
                    onChangeStatus={props.onChangeStatus}
                    onRemoveFromAssigned={props.onRemoveFromAssigned}
                    movies={props.movies}/>
            </div>
        </div>
    );
}

CinemaPage.propTypes = {
    handleClickAddNew: PropTypes.func,
};

export default CinemaPage;