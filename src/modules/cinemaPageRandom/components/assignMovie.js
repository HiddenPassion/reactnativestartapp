// @flow
// @flow
// @flow
// @flow
// @flow
// @flow
// @flow
// @flow
// @flow
// @flow
// @flow
// import React, { Component } from 'react';
// import enhanceWithClickOutside from 'react-click-outside';
// import { DebounceInput } from 'react-debounce-input';

// import classes from '../scss/assignMovie.scss';

// import { MovieDropDown } from '../../common/movieDropDown';

// class AssignMovie extends Component {
//   handleClickOutside = () => {
//     this.props.onCloseWindow();
//   };

//   onInputTitle = (event) => {
//     this.props.onChangeInputTitle({
//       cinemaId: this.props.cinemaId,
//       free: true,
//       title: event.target.value,
//     });
//   };

//   render() {
//     return (
//       <div
//         className={
//           `${this.props.isSidebar ? classes['assigning-window-sidebar'] : ''
//           } ${
//           classes['assigning-window']}`
//         }
//       >
//         <DebounceInput
//           debounceTimeout={300}
//           placeholder="Title"
//           className={classes['assigning-window__title-input']}
//           onChange={this.onInputTitle}
//         />
//         {!Array.isArray(this.props.movies) ? (
//           <MovieDropDown
//             onAssigningMovie={this.props.onAssigningMovie}
//             movies={this.props.movies}
//           />
//         ) : (
//           this.props.movies.map(movie => (
//             <MovieDropDown
//               onAssigningMovie={this.props.onAssigningMovie}
//               key={movie.id}
//               movies={movie}
//             />
//           ))
//         )}
//       </div>
//     );
//   }
// }

// export default enhanceWithClickOutside(AssignMovie);
