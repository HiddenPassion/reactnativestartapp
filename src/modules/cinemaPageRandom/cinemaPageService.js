// @flow
import axios from '../../utility/axios';
import { ASSIGN_MOVIE_URL } from '../cinemaList/utility/requesUrlConstants';

const assignMovie = (data: { movieId: string, cinemaId: string }) => axios.post(ASSIGN_MOVIE_URL + data.cinemaId, { movieId: data.movieId });

export default {
  assignMovie,
};
