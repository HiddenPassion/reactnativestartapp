// @flow
import * as actionTypes from './actionTypes';

const assignMovieStart = () => ({
  type: actionTypes.ASSIGN_MOVIE_START,
});

const assignMovieFail = (data: Object) => ({
  type: actionTypes.ASSIGN_MOVIE_FAIL,
  error: data,
});

const assignMovieSuccess = () => ({
  type: actionTypes.ASSIGN_MOVIE_SUCCESS,
});

export default {
  assignMovieStart,
  assignMovieFail,
  assignMovieSuccess,
};
