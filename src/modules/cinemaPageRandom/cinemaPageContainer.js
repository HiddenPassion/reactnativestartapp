// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import queryString from 'query-string';

import CinemaPage from './components/cinemaPage';
import * as asyncActionsMovies from '../movieList/thunk';
import * as asyncActionsCinemas from '../cinemaList/thunk';
import asyncActionsCinemaPage from './thunk';

import * as constants from '../roomList/utility/urlConstants';

class CinemaPageContainer extends Component {
  constructor() {
    super();

    this.state = {
      isAddingNew: false,
    };
  }

  componentWillMount() {
    if (queryString.parse(this.props.location.search).id) {
      this.cinemaId; // RECEIVE  ID  FROM PROPS
      this.props.getMovies({ cinemaId: this.cinemaId, isArchived: false }); // GETTING ASIGNED MOVIES
    } else {
      this.props.navigator.pop;
    }
  }

  handleClickAddNew = () => {
    const { getFreeMovies } = this.props;
    getFreeMovies({ cinemaId: this.cinemaId, free: true });
    this.setState(prevState => ({ isAddingNew: !prevState.isAddingNew }));
  };

  closeWindow = () => {
    this.setState(prevState => ({ isAddingNew: !prevState.isAddingNew }));
  };

  assignMovieToCinema = (event) => {
    this.props.assignMovie({ movieId: event.currentTarget.id, cinemaId: this.cinemaId });
    this.setState(prevState => ({ isAddingNew: !prevState.isAddingNew }));
  };

  changeMovieStatus = (event) => {
    this.props.changeMovieStatus({
      cinemaId: this.cinemaId,
      movieId: event.currentTarget.id,
      isArchived: event.currentTarget.value,
    });
  };

  removeFromAssigned = (event) => {
    this.props.removeFromAssigned({ relId: event.currentTarget.id, cinemaId: this.cinemaId });
  };

  render() {
    return (
      <CinemaPage
        isAddingNew={this.state.isAddingNew}
        onClickAddNew={this.handleClickAddNew}
        onCloseWindow={this.closeWindow}
        onAssigningMovie={this.assignMovieToCinema}
        onChangeStatus={this.changeMovieStatus}
        onRemoveFromAssigned={this.removeFromAssigned}
        onChangeInputTitle={this.props.getFreeMovies}
        cinema={this.props.cinema}
        movies={this.props.todayMovies}
        location={this.props.location}
        history={this.props.history}
        freeMovies={this.props.freeMovies}
      />
    );
  }
}

CinemaPageContainer.propTypes = {
  cinemaId: PropTypes.string,
};

const mapDispatchToProps = dispatch => ({
  getCinema: params => dispatch(asyncActionsCinemas.getCinemaById(params)),
  getMovies: params => dispatch(asyncActionsMovies.getMoviesWithFilter(params)),
  getFreeMovies: params => dispatch(asyncActionsMovies.fetchFreeMovies(params)),
  assignMovie: params => dispatch(asyncActionsCinemaPage.assignMovie(params)),
  changeMovieStatus: params => dispatch(asyncActionsMovies.changeMovieStatus(params)),
  removeFromAssigned: params => dispatch(asyncActionsMovies.removeFromAssigned(params)),
});

const mapStateToProps = state => ({
  cinema: state.cinemas.cinemasList,
  todayMovies: state.movies.moviesList,
  freeMovies: state.movies.freeMovies,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CinemaPageContainer);
