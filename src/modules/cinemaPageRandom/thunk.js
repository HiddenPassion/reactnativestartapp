// @flow
import cinemaPageService from './cinemaPageService';
import actions from './actions/cinemaPageAction';
import * as updateMovies from '../movieList/thunk';

const assignMovie = (data: Object) => (dispatch: Function) => {
  dispatch(actions.assignMovieStart());

  cinemaPageService
    .assignMovie(data)
    .then(() => {
      dispatch(actions.assignMovieSuccess());
      dispatch(updateMovies.getMoviesWithFilter({ cinemaId: data.cinemaId }));
    })
    .catch(() => {
      dispatch(actions.assignMovieFail());
    });
};

export default {
  assignMovie,
};
