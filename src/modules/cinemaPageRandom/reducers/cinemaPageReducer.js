// @flow
import clone from 'clone';

import * as actionTypes from '../actions/actionTypes';
import initialState from './initialState';

const reducer = (state: Object = initialState, action: Object) => {
  switch (action.type) {
    case actionTypes.ASSIGN_MOVIE_START:
      return assignMovieStart(state);
    case actionTypes.ASSIGN_MOVIE_FAIL:
      return assignMovieFail(state, action);
    case actionTypes.ASSIGN_MOVIE_SUCCESS:
      return assignMovieSuccess(state);
    default:
      return state;
  }
};

const assignMovieStart = state => ({
  ...clone(state),
  error: null,
});

const assignMovieFail = (state, action) => ({
  ...clone(state),
  error: action.error,
});

const assignMovieSuccess = state => ({
  ...clone(state),
  error: null,
  isAssigned: true,
});

export default reducer;
