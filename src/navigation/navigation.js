// @flow
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
import store from '../redux/store';

import StartScreen from '../modules/startScreen/startScreen';
import LoginScreen from '../modules/auth/logIn/loginContainer';
import { MovieListContainer as MovieListScreen } from '../modules/movieList';
import RegScreen from '../modules/auth/registration/registrationContainer';
import MovieSidebar from '../modules/movieSidebar/movieSidebarContainer';
import MovieScreen from '../modules/movie/movieContainer';
import AddMovieScreen from '../modules/addOrEditMovie/addMovieContainer/addMovieContainer';
import EditMovieScreen from '../modules/addOrEditMovie/editMovieContainer/editMovieContainer';
import CinemaListScreen from '../modules/cinemaList/cinemasListContainer';
import CinemaPageScreen from '../modules/cinemaPage/cinemaPage';
import CinemaCreationScreen from '../modules/cinemaCreation/сinemaСreationContainer';
import CinemaAdditionScreen from '../modules/addition/additionList';

Navigation.registerComponent('navigation.start', () => StartScreen, store, Provider);
Navigation.registerComponent('navigation.MovieListPage', () => MovieListScreen, store, Provider);
Navigation.registerComponent('navigation.login', () => LoginScreen, store, Provider);
Navigation.registerComponent('navigation.reg', () => RegScreen, store, Provider);
Navigation.registerComponent('navigation.movieSidebar', () => MovieSidebar, store, Provider);
Navigation.registerComponent('navigation.moviePage', () => MovieScreen, store, Provider);
Navigation.registerComponent('navigation.addMoviePage', () => AddMovieScreen, store, Provider);
Navigation.registerComponent('navigation.editMoviePage', () => EditMovieScreen, store, Provider);
Navigation.registerComponent('navigation.cinemaListPage', () => CinemaListScreen, store, Provider);
Navigation.registerComponent('navigation.cinemaPage', () => CinemaPageScreen, store, Provider);
Navigation.registerComponent(
  'navigation.cinemaCreationPage',
  () => CinemaCreationScreen,
  store,
  Provider,
);
Navigation.registerComponent(
  'navigation.cinemaAdditionPage',
  () => CinemaAdditionScreen,
  store,
  Provider,
);

const StartScreenNav = () => {
  Navigation.startSingleScreenApp({
    screen: {
      screen: 'navigation.start',
    },
    appStyle: {
      orientation: 'portrait',
    },
  });
};

export default StartScreenNav;
