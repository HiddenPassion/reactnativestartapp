// @flow
import { Navigation } from 'react-native-navigation';

const Login = () => {
  Navigation.startSingleScreenApp({
    screen: {
      screen: 'navigation.login',
      title: 'Login',
    },
    appStyle: {
      orientation: 'portrait',
    },
  });
};

export default Login;
