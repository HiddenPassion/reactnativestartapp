// @flow
import { Navigation } from 'react-native-navigation';

const Reg = () => {
  Navigation.startSingleScreenApp({
    screen: {
      screen: 'navigation.reg',
      title: 'Temp',
    },
    appStyle: {
      orientation: 'portrait',
    },
  });
};

export default Reg;
