// @flow
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

const Nav = () => {
  Promise.all([
    Icon.getImageSource('md-film', 30),
    Icon.getImageSource('md-map', 30),
    Icon.getImageSource('md-menu', 30),
    Icon.getImageSource('md-add', 40),
  ]).then((sources) => {
    Navigation.startTabBasedApp({
      tabs: [
        {
          screen: 'navigation.MovieListPage',
          label: 'Movies',
          title: 'Movies',
          icon: sources[0],
          navigatorButtons: {
            leftButtons: [
              {
                icon: sources[2],
                title: 'Menu',
                id: 'sideDrawerToggle',
              },
            ],
            rightButtons: [
              {
                icon: sources[3],
                title: 'Add',
                id: 'addButton',
              },
            ],
          },
        },
        {
          screen: 'navigation.cinemaListPage',
          label: 'Cinemas',
          title: 'Cinemas',
          icon: sources[1],
          navigatorButtons: {
            leftButtons: [
              {
                icon: sources[2],
                title: 'Menu',
                id: 'sideDrawerToggle',
              },
            ],
            rightButtons: [
              {
                icon: sources[3],
                title: 'Add',
                id: 'addButton',
              },
            ],
          },
        },
      ],
      tabsStyle: {
        tabBarSelectedButtonColor: 'orange',
      },
      appStyle: {
        tabBarSelectedButtonColor: 'orange',
      },
      drawer: {
        left: {
          screen: 'navigation.movieSidebar',
          fixedWidth: 850,
        },
      },
    });
  });
};

export default Nav;
