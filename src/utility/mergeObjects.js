// @flow
const mergeObjects = (oldObject: {}, updatedProperties: {}) => ({
  ...oldObject,
  ...updatedProperties,
});

export default mergeObjects;
