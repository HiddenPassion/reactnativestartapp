// @flow
export const CINEMAS = '/cinemas';
export const ADD_NEW_CINEMA = '/cinema/add';
export const EDIT_CINEMA = '/cinema/edit';
export const EDIT_CINEMA_ID = '/cinema/edit?id=';
export const CINEMA_PAGE_ID = '/cinema';

export const ASSIGNED_MOVIES = '/assigned_movies?id=';
export const ASSIGNED_MOVIES_CINEMAID = '/assigned_movies';

export const MOVIES = '/movies';
export const ADD_NEW_MOVIE = '/movie/add';
export const EDIT_MOVIE = '/movie/edit';

export const SESSIONS = '/sessions';
export const ADD_NEW_SESSION = '/session/add';
export const EDIT_SESSION = '/session/edit';

export const ADDITIONS = '/additions';

export const TEST = '/test';
export const ROOM = '/cinema/room';
export const ROOMS = '/cinema/rooms';
export const ROOMS_ID = '/cinema/rooms?id=';
export const ROOM_CREATION = '/cinema/rooms/add';
export const ROOM_CREATION_ID = '/cinema/rooms/add?id=';
export const ROOM_EDITION = '/cinema/rooms/edit';
export const ROOM_EDITION_ID = '/cinema/rooms/edit?roomId=';
