// @flow
export const BASE_URL = 'http://10.0.2.2:3000/';
export const NAME = 'name';
export const YEAR_RELEASE = 'yearRelease';
export const CITY = 'city';
export const ADDRESS = 'address';
