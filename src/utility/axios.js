// @flow
import { AsyncStorage } from 'react-native';
import axios from 'axios';
import * as url from './constants';

const instance = axios.create({
  baseURL: url.BASE_URL,
});

instance.interceptors.request.use(
  async (req) => {
    req.headers.isAdminApp = true;
    const token = await AsyncStorage.getItem('token');
    if (token && req.method !== 'OPTIONS') {
      req.headers.authorization = `Bearer ${token}`;
    }
    return req;
  },
  error => Promise.reject(error),
);

export default instance;
